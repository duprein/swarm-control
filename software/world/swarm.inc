# Desc: Swarm Robot Model
# Author: Vladimir Aleksandrov
# Date: 24 Jan 2010


define swarm-robot position
(
  color "gray50"
  drive "diff"		 	# Differential steering model.

  obstacle_return 1           	# Can hit things.
  laser_return 1                # reflects laser beams
  ranger_return 1             	# reflects sonar beams
  blobfinder_return 1         	# Seen by blobfinders

  localization "gps"

  size [1.2 1.2 0.6]
  origin [0 0 0 0 ]
  mass 0.5

  # body bottom
  #block
  #(
  #  points 8
  #  point[0] [ -0.25 -0.60 ]
  #  point[1] [  0.25 -0.60 ]
  #  point[2] [  0.60 -0.25 ]
  #  point[3] [  0.60  0.25 ]
  #  point[4] [  0.25  0.60 ]
  #  point[5] [ -0.25  0.60 ]
  #  point[6] [ -0.60  0.25 ]
  #  point[7] [ -0.60 -0.25 ]
  #  z [ 0 0.6 ]
  #  #color "yellow"
  #)

  bitmap "bitmaps/robot.png"
  gui_outline 0

  # distance sensor
  #block
  #(
  #  points 4
  #  point[0] [ 0.40 -0.5]
  #  point[1] [ 0.50 -0.4]
  #  point[2] [ 0.40 -0.3]
  #  point[3] [ 0.30 -0.4]
  #  z [0.3 0.4]
  #  #color "brown"
  #)
  # distance sensor
  #block
  #(
  #  points 4
  #  point[0] [ 0.63 -0.05 ]
  #  point[1] [ 0.63  0.05 ]
  #  point[2] [ 0.53  0.05 ]
  #  point[3] [ 0.53 -0.05 ]
  #  z [0.3 0.4]
  #  #color "brown"
  #)
  # distance sensor
  #block
  #(
  #  points 4
  #  point[0] [ 0.40 0.50 ]
  #  point[1] [ 0.50 0.40 ]
  #  point[2] [ 0.40 0.30 ]
  #  point[3] [ 0.30 0.40 ]
  #  z [0.3 0.4]
  #  #color "brown"
  #)

  swarm_rangers( pose [0 0 -0.3 0] )
  swarm_fidfind( pose [0.61 0 -0.5 0] )
  robot_fw_finder( pose [0.61 0 -0.5 0] )
  robot_bw_finder( pose [-0.61 0 -0.5 180] )
)

define swarm_rangers ranger
(
    scount 8
    spose[0] [ 0.6   0.00    0.0 ]
    spose[1] [ 0.55  0.23   22.5 ]
    spose[2] [ 0.23  0.55   67.5 ]
    spose[3] [-0.23  0.55  112.5 ]
    spose[4] [-0.6   0.0   180.0 ]
    spose[5] [-0.23 -0.55 -112.5 ]
    spose[6] [ 0.23 -0.55  -67.5 ]
    spose[7] [ 0.55 -0.23  -22.5 ]

    ssize [0.05 0.05]

    sview [0.0 3.0 10]

    alwayson 1
)

define swarm_fidfind fiducial
(
 # fiducialfinder properties
    range_min 0.0
    range_max 4.0
    range_max_id 4.0
    fov 20
    fiducial_key 0

# model properties
    size [ 0.1 0.1 0.1 ]
)

define robot_fw_finder fiducial
(
 # robot_finder properties
    range_min 0.0
    range_max 4.0
    range_max_id 4.0
    fov 120
    fiducial_key 1

# model properties
    size [ 0.1 0.1 0.1 ]
)

define robot_bw_finder fiducial
(
 # robot_finder properties
    range_min 0.0
    range_max 4.0
    range_max_id 4.0
    fov 120
    fiducial_key 1

# model properties
    size [ 0.1 0.1 0.1 ]
)
