#include "swarm_control.h"

player_point_2d_t   pnt[GRID_QTY];
player_color        col;
unsigned int free_counter;

int main(int argc, char *argv[])
{

    // ***********  LOCAL VARIABLES & STRUCTURES ****************
    unsigned int i,j;

    // ******************* INITIALIZING ********************

    finish_flag=false;
    robot_client = new PlayerClient("localhost", 6665);

    for (i=0;i<ROBOT_QTY;i++)
    {
        robot[i] = new Robot(i, robot_client);
    }

    SetInitPose(OK);
    //SetInitPose(OD);
    //SetInitPose(DK);

    for (i=0;i<ROBOT_QTY;i++)
    {
        robot[i]->InitPosition();
    }

    graph2d = new Graphics2dProxy(robot_client, 0);

    world.step=0;
    world.epoch=0;

    for (i=0;i<ROBOT_QTY;i++)
    {
        rlist[i].x=robot[i]->GetX();
        rlist[i].y=robot[i]->GetY();
        rlist[i].free=1;
        printf("\n rlist.x=%f, rlist.y=%f", rlist[i].x, rlist[i].y);
    }

    printf("\n\n---------------------------------------------\n");

    for (i=0;i<GRID_V;i++)
    {
        for (j=0;j<GRID_H;j++)
        {
            if(RANDOM_TLIST==0)
            {
                tlist[i*GRID_H+j].y=-(GRID_V-1)/2+i;
                tlist[i*GRID_H+j].x=-(GRID_H-1)/2+j;
                tlist[i*GRID_H+j].status=1;
                tlist[i*GRID_H+j].visited=0;
            }
            else
            {
                tlist[i*GRID_H+j].y=-(GRID_V-1)/2+i;
                tlist[i*GRID_H+j].x=-(GRID_H-1)/2+j;
                srand(time(NULL)+clock()+rand());
                printf("random=%d \n", rand()%100);
                if((rand()%100)>50)
                {
                    tlist[i*GRID_H+j].status=1;
                    tlist[i*GRID_H+j].visited=0;
                }
                else
                {
                    tlist[i*GRID_H+j].status=-1;
                    tlist[i*GRID_H+j].visited=1;
                }
            }
            //printf("\n%d: tlist.x=%d, tlist.y=%d", i*20+j, tlist[i*20+j].x, tlist[i*20+j].y);
        }
    }
    tlist_counter_init=0;
    for (i=0;i<GRID_V;i++)
    {
        for (j=0;j<GRID_H;j++)
        {
            if(tlist[i*GRID_H+j].visited==0)
                tlist_counter_init++;
        }
    }

    player_point_2d_t   point[4];
    player_color        color;

    for (i=0;i<GRID_QTY;i++)
    {
        point[0].px=tlist[i].x-0.1;
        point[0].py=tlist[i].y-0.1;

        point[1].px=tlist[i].x+0.1;
        point[1].py=tlist[i].y-0.1;

        point[2].px=tlist[i].x+0.1;
        point[2].py=tlist[i].y+0.1;

        point[3].px=tlist[i].x-0.1;
        point[3].py=tlist[i].y+0.1;

        color.green=127;

        graph2d->DrawPolygon(point, 4, 1, color);
    }


    // ************************ MAIN LOOP ************************
    ftime(&tp_start);
    log_out = fopen("log2.txt","a");

    for (;;)
    {
        /* this blocks until new data comes; 10Hz by default */
        printf("\nFull Cycle Time of %d.%d: %1.3f sec",world.epoch,world.step,((double)tp2.time+(double)tp2.millitm/1000.0)-((double)tp1.time+(double)tp1.millitm/1000.0));
        ftime(&tp1);

        robot_client->Read();

        printf("\nControl Routine Time of %d.%d: %1.3f sec",world.epoch,world.step,((double)tp4.time+(double)tp4.millitm/1000.0)-((double)tp3.time+(double)tp3.millitm/1000.0));
        ftime(&tp3);


        printf("\n grid update starts");

        GridUpdate();

        if(finish_flag)
        {
            FinishLog();
            return 0;
        }

        printf("\n");
        /*for(i=0;i<GRID_QTY;i++)
        {
            for(j=0;j<ROBOT_QTY;j++)
            {
                printf("%2.2f   ", eff[j][i]);
            }
            printf("\n");
        }*/

        printf("\nRobot Control Cycle starts");
        free_counter=0;
        for (i=0;i<ROBOT_QTY;i++)
        {
            robot[i]->StateUpdater();
            ControlLoop(i);
            robot[i]->Control();
            robot[i]->AddTravelPath();
            CollisionHandler(i);
        }
        ftime(&tp4);
        printf("\nRobot Control Cycle ends");

        world.step++;

        ftime(&tp2);
    }
}

void SetInitPose(unsigned int ip)
{
    switch(ip)
    {

    case OK:
    robot[0]->SetPose(-8,5,0);
    robot[1]->SetPose(-8,3,0);
    robot[2]->SetPose(-8,1,0);
    robot[3]->SetPose(-8,-1,0);
    robot[4]->SetPose(-8,-3,0);
    robot[5]->SetPose(-8,-5,0);
    break;

    case OD:
    robot[0]->SetPose(-8,-6,0);
    robot[1]->SetPose(-5,-6,0);
    robot[2]->SetPose(-2,-6,0);
    robot[3]->SetPose(2,-6,0);
    robot[4]->SetPose(5,-6,0);
    robot[5]->SetPose(8,-6,0);
    break;

    case DK:
    robot[0]->SetPose(-8,5,0);
    robot[1]->SetPose(-8,0,0);
    robot[2]->SetPose(-8,-5,0);
    robot[3]->SetPose(8,5,0);
    robot[4]->SetPose(8,0,0);
    robot[5]->SetPose(8,-5,0);
    break;
    }
}

void ControlLoop(unsigned int i)
{
    //Init the tactic
    int j;
    if (robot[i]->GetTactic()==NONE)
    {
        //init target points
        for (j=0;j<GRID_QTY;j++)
        {
            if((tlist[j].x==rlist[i].x)&&(tlist[j].y==rlist[i].y))
            {
                tlist[j].status=0;
            }
        }
        robot[i]->SelectTactic(KAC_ALGO);
        printf("\n%s: start KAC algo.", robot[i]->m_name);
    }
    if(robot[i]->GetTactic()==GO_TO)
    {
        if((fabs((float)rlist[i].targetX-rlist[i].x)<0.1)&&(fabs((float)rlist[i].targetY-rlist[i].y)<0.1))
        {
            free_counter++;
            rlist[i].attarget=1;
            printf("\n%s: at target point.", robot[i]->m_name);
        }

        if(free_counter==6)
        {
            for(j=0;j<ROBOT_QTY;j++)
            {
                robot[j]->SelectTactic(KAC_ALGO);
                rlist[j].free=1;
            }
        }
    }
    printf("\n%s: Current tactic is: %d", robot[i]->m_name, robot[i]->GetTactic());
}

void GridUpdate(void)
{
    unsigned int i,j;
    player_point_2d_t   point[4];
    player_color        color;

    for (i=0;i<ROBOT_QTY;i++)
    {
        rlist[i].x=robot[i]->GetX();
        rlist[i].y=robot[i]->GetY();
        printf("\n rlist.x=%f, rlist.y=%f", rlist[i].x, rlist[i].y);
    }

    graph2d->Clear();
    for (i=0;i<GRID_QTY;i++)
    {
        for(j=0;j<ROBOT_QTY;j++)
        {
            if((fabs((float)tlist[i].x-rlist[j].x)<COV_RADIUS)&&(fabs((float)tlist[i].y-rlist[j].y)<COV_RADIUS)&&(!tlist[i].visited))
            {
                tlist[i].visited=1;
                visited_qty++;
                if(tlist[i].status>0)
                    tlist[i].status=-1;
            }
        }
        if (!tlist[i].visited)
        {
            point[0].px=tlist[i].x-0.1;
            point[0].py=tlist[i].y-0.1;

            point[1].px=tlist[i].x+0.1;
            point[1].py=tlist[i].y-0.1;

            point[2].px=tlist[i].x+0.1;
            point[2].py=tlist[i].y+0.1;

            point[3].px=tlist[i].x-0.1;
            point[3].py=tlist[i].y+0.1;

            color.green=127;

            graph2d->DrawPolygon(point, 4, 1, color);
        }
    }
    printf("\nVisitedQTY=%d", visited_qty);
    if(visited_qty==tlist_counter_init)
    {
        finish_flag=true;
    }

}

void CollisionHandler(unsigned int i)
{
    if (robot[i]->m_stallSteps>3)
    {
        /*robot[i]->SelectTactic(GO_BACKWARD);
        std::cout<<"stop!\n";
        robot[i]->m_stallSteps=0;
        robot[i]->m_stallCounter++;*/
        finish_flag=true;
    }
}

void FinishLog(void)
{
    ftime(&tp_end);
    time(&finish_time);
    //fprintf(log_out, "----------------------\n %s \n", ctime(&finish_time));
    //fprintf(log_out, "Coverage time: %3.3f sec\n",((double)tp_end.time+(double)tp_end.millitm/1000.0)-((double)tp_start.time+(double)tp_start.millitm/1000.0));
    fprintf(log_out, "%3.3f ; ",((double)tp_end.time+(double)tp_end.millitm/1000.0)-((double)tp_start.time+(double)tp_start.millitm/1000.0));
    int i,j;
    tlist_counter_final=0;
    for (i=0;i<GRID_V;i++)
    {
        for (j=0;j<GRID_H;j++)
        {
            if(tlist[i*GRID_H+j].visited==0)
                tlist_counter_final++;
        }
    }
    //fprintf(log_out, "Initial target qty: %d\n", tlist_counter_init);
    //fprintf(log_out, "Covered target qty: %d\n", tlist_counter_init-tlist_counter_final);
    //fprintf(log_out, "Complete: %2.2f%%\n", ((float)tlist_counter_init-(float)tlist_counter_final)/(float)tlist_counter_init);
    fprintf(log_out, "%d ; ", tlist_counter_init);
    fprintf(log_out, "%d ; ", tlist_counter_init-tlist_counter_final);
    fprintf(log_out, "%2.2f ; ", ((float)tlist_counter_init-(float)tlist_counter_final)/(float)tlist_counter_init);
    printf("3");
    double travel_path=0;
    for(i=0;i<ROBOT_QTY;i++)
    {
        travel_path+=robot[i]->GetTravelPath();
    }
    //fprintf(log_out, "Total travel path: %3.3f\n", travel_path);
    //fprintf(log_out, "Avg length to target: %3.3f\n", travel_path/((float)tlist_counter_init-(float)tlist_counter_final));
    fprintf(log_out, "%3.3f ; ", travel_path);
    fprintf(log_out, "%3.3f \n", travel_path/((float)tlist_counter_init-(float)tlist_counter_final));
}
