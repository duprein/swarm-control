# Desc: Swarm Robot Model
# Author: Vladimir Aleksandrov
# Date: 24 Jan 2010


define swarm-robot position
(
  color "gray50"
  drive "diff"		 	# Differential steering model.

  obstacle_return 1           	# Can hit things.
  laser_return 1                # reflects laser beams
  ranger_return 1             	# reflects sonar beams
  blobfinder_return 1         	# Seen by blobfinders

  localization "gps"
  localization_origin [0 0 0]
  watchdog_timeout -1.0


  #size [1.2 1.2 0.6]

  #size [0.2 0.2 0.6]
  #size [0.4 0.4 0.6]
  #size [0.6 0.6 0.6]
  #size [0.8 0.8 0.6]
  #size [1.0 1.0 0.6]
  size [1.2 1.2 0.6]
  #size [1.4 1.4 0.6]

  origin [0 0 0 0 ]
  mass 0.5

  bitmap "bitmaps/robot.png"
  gui_outline 0

  swarm_rangers( pose [0 0 -0.5 0] )
  swarm_rangers( pose [0 0 -0.5 0] )
  swarm_fidfind( pose [0.61 0 -0.5 0] )
  robot_fw_finder( pose [0.61 0 -0.5 0] )
  robot_bw_finder( pose [-0.61 0 -0.5 180] )
)

define my_sonar sensor
(
  # define the size of each transducer [xsize ysize] in meters
  size [0.1 0.1 0.1 ]
  # define the field of view of each transducer [range_min range_max view_angle]
  view [0.1 4.0 10]  # min (m), max (m), field of view (deg)
 )

define swarm_rangers ranger
(
    my_sonar( pose [ 0.6   0.00  0.0    0.0 ] )
    my_sonar( pose [ 0.55  0.23  0.0   22.5 ] )
    my_sonar( pose [ 0.23  0.55  0.0   67.5 ] )
    my_sonar( pose [-0.23  0.55  0.0  112.5 ] )
    my_sonar( pose [-0.6   0.0   0.0  180.0 ] )
    my_sonar( pose [-0.23 -0.55  0.0 -112.5 ] )
    my_sonar( pose [ 0.23 -0.55  0.0  -67.5 ] )
    my_sonar( pose [ 0.55 -0.23  0.0  -22.5 ] )
)

#define swarm_rangers ranger
#(
#    scount 8
#    spose[0] [ 0.6   0.00    0.0 ]
#    spose[1] [ 0.55  0.23   22.5 ]
#    spose[2] [ 0.23  0.55   67.5 ]
#    spose[3] [-0.23  0.55  112.5 ]
#    spose[4] [-0.6   0.0   180.0 ]
#    spose[5] [-0.23 -0.55 -112.5 ]
#    spose[6] [ 0.23 -0.55  -67.5 ]
#    spose[7] [ 0.55 -0.23  -22.5 ]
#
#    ssize [0.05 0.05]
#
#    sview [0.0 3.0 10]
#)

define swarm_fidfind fiducial
(
 # fiducialfinder properties
    range_min 0.0
    range_max 4.0
    range_max_id 4.0
    fov 20
    fiducial_key 0

# model properties
    size [ 0.1 0.1 0.1 ]
)

define robot_fw_finder fiducial
(
 # robot_finder properties
    range_min 0.0
    range_max 2.0
    range_max_id 2.0
    fov 160
    fiducial_key 1

# model properties
    size [ 0.1 0.1 0.1 ]
)

define robot_bw_finder fiducial
(
 # robot_finder properties
    range_min 0.0
    range_max 2.0
    range_max_id 2.0
    fov 160
    fiducial_key 1

# model properties
    size [ 0.1 0.1 0.1 ]
)
