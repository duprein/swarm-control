#include "swarm_control.h"

#define NDIM    2

#define POLY_L  10
#define POLY_W  7.5

player_point_2d_t   pnt[GRID_QTY];
player_color        col;
unsigned int free_counter, workpoints_open;
bool stopped=false;
player_point_2d_t   poly[10];

int main(int argc, char *argv[])
{

    // ***********  LOCAL VARIABLES & STRUCTURES ****************
    unsigned int i;

    // ******************* INITIALIZING ********************

    finish_flag=false;
    robot_client = new PlayerClient("localhost", 6665);
    workpoints_client = new PlayerClient("localhost", 6664);

    for (i=0; i<ROBOT_QTY; i++)
    {
        robot[i] = new Robot(i, robot_client);
        robot_client->ReadIfWaiting();
        workpoints_client->ReadIfWaiting();
        blackboard.walk_side[i]=rand_interval(0,1);
    }



    for (i=0;i<WORKPOINT_QTY;i++)
    {
        printf("\n in WP init");
        workpoint[i] = new Workpoint(i, workpoints_client, robot_client);
        robot_client->ReadIfWaiting();
        workpoints_client->ReadIfWaiting();
    }

    SetInitPose(P_OK);
    //SetInitPose(OD);
    //SetInitPose(DK);
    //SetInitPose(LL);

    //SetInitPose(MA);
    robot_client->Read();

    for (i=0; i<ROBOT_QTY; i++)
    {
        robot[i]->InitPosition();
        robot_client->Read();
    }

    graph2d = new Graphics2dProxy(robot_client, 0);

    world.step=0;
    world.epoch=0;

    InitRlistTlist();
    area_divided=false;

    // ************************ MAIN LOOP ************************
    ftime(&tp_start);
    log_out = fopen("log2.txt","a");

    for (;;)
    {
        /* this blocks until new data comes; 10Hz by default */
        printf("\nFull Cycle Time of %d.%d: %1.3f sec",world.epoch,world.step,((double)tp2.time+(double)tp2.millitm/1000.0)-((double)tp1.time+(double)tp1.millitm/1000.0));
        ftime(&tp1);

        robot_client->Read();
        workpoints_client->Read();

        printf("\nControl Routine Time of %d.%d: %1.3f sec",world.epoch,world.step,((double)tp4.time+(double)tp4.millitm/1000.0)-((double)tp3.time+(double)tp3.millitm/1000.0));
        ftime(&tp3);
//_______________________________________________________________________________
        //This is only for Voronoi division coverage
        /*if(!area_divided)
        {
            VoronoiDivide();
        }
        else
        {
            while (!inputAvailable())
            {
                robot_client->Read();
                GridUpdate();
            }
            for(i=0; i<ROBOT_QTY; i++)
            {
                robot[i]->SelectTactic(STOP);
                robot[i]->Control();
            }
            finish_flag=true;
        }

        GridUpdate();
        */
//_______________________________________________________________________________
        //This is only for TSP/KA coverage
        /*else if(!finish_flag)
        {
            for(i=0; i<ROBOT_QTY; i++)
            {
                if(robot[i]->GetTactic()!=TSP)
                {
                    robot[i]->SelectTactic(STOP);
                    robot[i]->Control();
                    printf("\n swarm %i: Subspace Order = %i", i, robot[i]->m_subspace_order);
                    robot[i]->DivideTargets();
                }
                robot[i]->DivideTargets();
                robot[i]->SelectTactic(TSP);
                robot[i]->Control();
            }

        }
        GridUpdate();
        */
//_______________________________________________________________________________
        //This is only for cooperative_search

        free_counter=0;

        for (i=0;i<ROBOT_QTY;i++)
        {

            robot[i]->StateUpdater();//0.2s

            ControlLoop(i);//0.00s
            //ControlLoopRandomWalk(i);

            robot[i]->Control();//0.00s

            robot[i]->AddTravelPath();


        }

        workpoints_open=0;
        for (i=0;i<WORKPOINT_QTY;i++)
        {
            workpoint[i]->StateUpdater();
            if(workpoint[i]->IsOpen()) workpoints_open++;
        }
        ftime(&tp4);

//_______________________________________________________________________________

        /*if(finish_flag)
        {
            FinishLog();
            return 0;
        }*/

        if(workpoints_open==0)
        {
            if(stopped)
            {
                FinishLog();
                WaitForAKey();
                return 0;
            }
            for (i=0;i<ROBOT_QTY;i++)
            {
                robot[i]->SelectTactic(STOP);
                stopped=true;
            }
        }

        printf("\n");

        world.step++;

        ftime(&tp2);
    }
}

void InitRlistTlist(void)
{
    int i,j;
    for (i=0; i<ROBOT_QTY; i++)
    {
        rlist[i].x=robot[i]->GetX();
        rlist[i].y=robot[i]->GetY();
        rlist[i].free=1;
        //printf("\n rlist.x=%f, rlist.y=%f", rlist[i].x, rlist[i].y);
    }

    for (i=0; i<GRID_V; i++)
    {
        for (j=0; j<GRID_H; j++)
        {
            if(RANDOM_TLIST==0)
            {
                tlist[i*GRID_H+j].y=-(GRID_V-1)/2+i;
                tlist[i*GRID_H+j].x=-(GRID_H-1)/2+j;
                tlist[i*GRID_H+j].status=1;
                tlist[i*GRID_H+j].visited=0;
            }
            else
            {
                tlist[i*GRID_H+j].y=-(GRID_V-1)/2+i;
                tlist[i*GRID_H+j].x=-(GRID_H-1)/2+j;
                srand(time(NULL)+clock()+rand());
                //printf("random=%d \n", rand()%100);
                if((rand()%100)>50)
                {
                    tlist[i*GRID_H+j].status=1;
                    tlist[i*GRID_H+j].visited=0;
                }
                else
                {
                    tlist[i*GRID_H+j].status=-1;
                    tlist[i*GRID_H+j].visited=1;
                }
            }
            //printf("\n%d: tlist.x=%d, tlist.y=%d", i*20+j, tlist[i*20+j].x, tlist[i*20+j].y);
        }
    }
    tlist_counter_init=0;
    for (i=0; i<GRID_V; i++)
    {
        for (j=0; j<GRID_H; j++)
        {
            if(tlist[i*GRID_H+j].visited==0)
                tlist_counter_init++;
        }
    }
}

double angle( int x1, int y1, int x2, int y2)
{
    double t = (x1*x2+y1*y2)/(sqrt((double)x1*x1+y1*y1)*sqrt((double)x2*x2+y2*y2));
    if     (t<-1) t=-1;
    else if(t> 1) t= 1;
    return acos(t);
}

void VoronoiDivide(void)
{
    int *g_degree;
    int *g_face;
    int g_num;
    int *g_start;
    double g_xy[ROBOT_QTY*2];
    double *i_xy;
    int v_num;
    int i_num;
    double *v_xy;
    g_num=ROBOT_QTY;
    bool reached_centroids=true;

    double area[ROBOT_QTY];
    player_point_2d_t centroid[ROBOT_QTY];
    //player_point_2d_t p[12];

    int i,j;

    for(i=0; i<ROBOT_QTY; i++)
    {
        g_xy[2*i]=robot[i]->GetX();
        g_xy[2*i+1]=robot[i]->GetY();
        printf("robot[%d] at [%2.1f,%2.1f]\n", i, g_xy[2*i], g_xy[2*i+1]);
    }
    /*graph2d->Clear();
    graph2d->Color(0,0,127,0);

    for (i=0; i<6; i++)
    {
        p[i].px=g_xy[2*i];
        //g_xy[2*i]=g_xy[2*i]+10;
        p[i].py=g_xy[2*i+1];
        //g_xy[2*i+1]=g_xy[2*i+1]+10;
    }
    graph2d->DrawPoints(p, 6);*/

    i_xy = new double[NDIM*g_num];
    g_degree = new int[g_num];
    g_face = new int[6*g_num];
    g_start = new int[g_num];
    v_xy = new double[NDIM*2*g_num];

    voronoi_data ( g_num, g_xy, g_degree, g_start, g_face, &v_num, v_xy,&i_num, i_xy );
    //printf("\nv_num=%d", v_num);

    player_point_2d_t vp[g_num];
    gpc_polygon *poly_border, *voronoi_cell, *voronoi_clipped;
    poly_border = new gpc_polygon;
    voronoi_cell = new gpc_polygon[ROBOT_QTY];
    voronoi_clipped = new gpc_polygon[ROBOT_QTY];

    for (i=0; i<ROBOT_QTY; i++)
    {
        //printf("Cell %d:\n", i);
        for(j=0; j<g_degree[i]; j++)
        {
            //printf("Vertex num %d type ", g_face[g_start[i]-1+j]);
            if(g_face[g_start[i]-1+j]>0)
            {
                //printf(" norm: %3.2f, %3.2f\n", v_xy[2*(g_face[g_start[i]-1+j]-1)], v_xy[2*(g_face[g_start[i]-1+j]-1)+1]);
                double x1, y1;
                x1=v_xy[2*(g_face[g_start[i]-1+j]-1)];
                y1=v_xy[2*(g_face[g_start[i]-1+j]-1)+1];

                vp[j].px=x1;
                vp[j].py=y1;
            }
            else
            {
                printf(" inf: %3.2f, %3.2f\n", i_xy[2*(-1*g_face[g_start[i]-1+j]-v_num-1)], i_xy[2*(-1*g_face[g_start[i]-1+j]-v_num-1)+1]);
                double x1, y1;
                x1=i_xy[2*(-1*g_face[g_start[i]-1+j]-v_num-1)];
                y1=i_xy[2*(-1*g_face[g_start[i]-1+j]-v_num-1)+1];
                vp[j].px=x1*100+0.1;
                vp[j].py=y1*100+0.1;
            }
        }

        poly_border->num_contours=1;
        poly_border->hole=(int *)malloc(poly_border->num_contours*sizeof(int));
        poly_border->hole[0] = FALSE;
        poly_border->contour=(gpc_vertex_list *)malloc(poly_border->num_contours * sizeof(gpc_vertex_list));


        //Polygon description for bitmaps/polygon5_wo_obst.png

        poly_border->contour[0].num_vertices=4;
        poly_border->contour[0].vertex=(gpc_vertex *)malloc(poly_border->contour[0].num_vertices * sizeof(gpc_vertex));

        poly_border->contour[0].vertex[0].x=-10;
        poly_border->contour[0].vertex[0].y=-7.5;
        poly_border->contour[0].vertex[1].x=-10;
        poly_border->contour[0].vertex[1].y=7.5;
        poly_border->contour[0].vertex[2].x=10;
        poly_border->contour[0].vertex[2].y=7.5;
        poly_border->contour[0].vertex[3].x=10;
        poly_border->contour[0].vertex[3].y=-7.5;


        //Polygon description for bitmaps/polygon6_wo_obst.png
/*
        poly_border->contour[0].num_vertices=7;
        poly_border->contour[0].vertex=(gpc_vertex *)malloc(poly_border->contour[0].num_vertices * sizeof(gpc_vertex));

        poly_border->contour[0].vertex[0].x=-9.86;
        poly_border->contour[0].vertex[0].y=-7.45;

        poly_border->contour[0].vertex[1].x=-9.86;
        poly_border->contour[0].vertex[1].y=4.64;

        poly_border->contour[0].vertex[2].x=-8.09;
        poly_border->contour[0].vertex[2].y=7.36;

        poly_border->contour[0].vertex[3].x=7.45;
        poly_border->contour[0].vertex[3].y=7.36;

        poly_border->contour[0].vertex[4].x=9.91;
        poly_border->contour[0].vertex[4].y=5.36;

        poly_border->contour[0].vertex[5].x=8.50;
        poly_border->contour[0].vertex[5].y=1.14;

        poly_border->contour[0].vertex[6].x=3.45;
        poly_border->contour[0].vertex[6].y=-7.36;
*/

        /*Polygon description for bitmaps/polygon7_wo_obst.png

        poly_border->contour[0].num_vertices=8;
        poly_border->contour[0].vertex=(gpc_vertex *)malloc(poly_border->contour[0].num_vertices * sizeof(gpc_vertex));

        poly_border->contour[0].vertex[0].x=-9.82;
        poly_border->contour[0].vertex[0].y=-4.00;

        poly_border->contour[0].vertex[1].x=-9.91;
        poly_border->contour[0].vertex[1].y=2.55;

        poly_border->contour[0].vertex[2].x=-6.73;
        poly_border->contour[0].vertex[2].y=7.41;

        poly_border->contour[0].vertex[3].x=8.95;
        poly_border->contour[0].vertex[3].y=7.41;

        poly_border->contour[0].vertex[4].x=9.95;
        poly_border->contour[0].vertex[4].y=6.45;

        poly_border->contour[0].vertex[5].x=9.86;
        poly_border->contour[0].vertex[5].y=-1.36;

        poly_border->contour[0].vertex[6].x=8.00;
        poly_border->contour[0].vertex[6].y=-7.41;

        poly_border->contour[0].vertex[7].x=4.14;
        poly_border->contour[0].vertex[7].y=-7.41;*/

        // Polygon description for bitmaps/polygon8_wo_obst.png
/*
        poly_border->contour[0].num_vertices=3;
        poly_border->contour[0].vertex=(gpc_vertex *)malloc(poly_border->contour[0].num_vertices * sizeof(gpc_vertex));

        poly_border->contour[0].vertex[0].x=-9.88;
        poly_border->contour[0].vertex[0].y=-7.41;
        poly_border->contour[0].vertex[1].x=-0.02;
        poly_border->contour[0].vertex[1].y=7.32;
        poly_border->contour[0].vertex[2].x=9.89;
        poly_border->contour[0].vertex[2].y=-7.41;
*/


        (voronoi_cell+i)->num_contours=1;
        (voronoi_cell+i)->hole=(int *)malloc((voronoi_cell+i)->num_contours * sizeof(int));
        (voronoi_cell+i)->hole[0] = FALSE;
        (voronoi_cell+i)->contour=(gpc_vertex_list *)malloc((voronoi_cell+i)->num_contours * sizeof(gpc_vertex_list));
        (voronoi_cell+i)->contour[0].num_vertices=g_degree[i];
        (voronoi_cell+i)->contour[0].vertex=(gpc_vertex *)malloc((voronoi_cell+i)->contour[0].num_vertices * sizeof(gpc_vertex));

        for(j=0; j<g_degree[i]; j++)
        {
            (voronoi_cell+i)->contour[0].vertex[j].x=vp[j].px;
            (voronoi_cell+i)->contour[0].vertex[j].y=vp[j].py;
            printf ("%d\n", j);
        }

        gpc_polygon_clip(GPC_INT, (voronoi_cell+i), poly_border, (voronoi_clipped+i));

        printf("Initial contour num: %d ; Vert num: %d\n", (voronoi_cell+i)->num_contours, (voronoi_cell+i)->contour[0].num_vertices);
        printf("Border contour num: %d ; Vert num: %d\n", poly_border->num_contours, poly_border->contour[0].num_vertices);
        printf("Clipped contour num: %d", (voronoi_clipped+i)->num_contours);

        if((voronoi_clipped+i)->num_contours>0)
        {
            printf("Vert num: %d", (voronoi_clipped+i)->contour[0].num_vertices);
            player_point_2d_t vpn[(voronoi_clipped+i)->contour[0].num_vertices+1];
            for (j=0; j<(voronoi_clipped+i)->contour[0].num_vertices; j++)
            {
                vpn[j].px=(voronoi_clipped+i)->contour[0].vertex[j].x;
                vpn[j].py=(voronoi_clipped+i)->contour[0].vertex[j].y;
            }
            vpn[(voronoi_clipped+i)->contour[0].num_vertices].px=vpn[0].px;
            vpn[(voronoi_clipped+i)->contour[0].num_vertices].py=vpn[0].py;
            //graph2d->Color(255,0,0,0);
            //graph2d->DrawPolyline(vpn, (voronoi_clipped+i)->contour[0].num_vertices+1);
            //graph2d->Color(0,255,0,0);
            //graph2d->DrawPoints(vpn, (voronoi_clipped+i)->contour[0].num_vertices+1);

            //vpn[N]=vpn[0]...... MUST BE!

            //Calculating area and centroid of the polygon
            area[i]=0;
            centroid[i].px=0;
            centroid[i].py=0;

            //for all except the last one
            for(j=0; j<(voronoi_clipped+i)->contour[0].num_vertices; j++)
            {
                area[i]+=(vpn[j].px*vpn[j+1].py-vpn[j+1].px*vpn[j].py);
                centroid[i].px+=(vpn[j].px*vpn[j+1].py-vpn[j+1].px*vpn[j].py)*(vpn[j].px+vpn[j+1].px);
                centroid[i].py+=(vpn[j].px*vpn[j+1].py-vpn[j+1].px*vpn[j].py)*(vpn[j].py+vpn[j+1].py);
            }
            //and the last one separately too loop the count till the beginning point
            area[i]+=(vpn[(voronoi_clipped+i)->contour[0].num_vertices].px*vpn[0].py-vpn[0].px*vpn[(voronoi_clipped+i)->contour[0].num_vertices].py);
            centroid[i].px+=(vpn[(voronoi_clipped+i)->contour[0].num_vertices].px*vpn[0].py-vpn[0].px*vpn[(voronoi_clipped+i)->contour[0].num_vertices].py)*(vpn[(voronoi_clipped+i)->contour[0].num_vertices].px+vpn[0].px);
            centroid[i].py+=(vpn[(voronoi_clipped+i)->contour[0].num_vertices].px*vpn[0].py-vpn[0].px*vpn[(voronoi_clipped+i)->contour[0].num_vertices].py)*(vpn[(voronoi_clipped+i)->contour[0].num_vertices].py+vpn[0].py);

            //perform additional operations
            area[i]=area[i]/2.0;
            centroid[i].px=centroid[i].px/(6.0*area[i]);
            centroid[i].py=centroid[i].py/(6.0*area[i]);
            printf("area[%d]=%3.2f\n", i, area[i]);
            printf("centroid[%d].px=%3.2f\n", i, centroid[i].px);
            printf("centroid[%d].py=%3.2f\n", i, centroid[i].py);
            graph2d->Color(0,255,255,0);
            graph2d->DrawPoints(centroid, ROBOT_QTY);

            if (fabs(centroid[i].px-robot[i]->GetX())>DIVISION_TOLERANCE || fabs(centroid[i].py-robot[i]->GetY())>DIVISION_TOLERANCE)
            {
                robot[i]->MoveTo(centroid[i].px, centroid[i].py, angle(robot[i]->GetX(),robot[i]->GetY(), 1, 0)+angle(robot[i]->GetX(),robot[i]->GetY(), centroid[i].px, centroid[i].py));
                reached_centroids=false;
            }

            robot[i]->AddTravelPath();

            //---------------------------------
            robot[i]->m_subspace_order=(voronoi_clipped+i)->contour[0].num_vertices+1;
            for (j=0; j<(voronoi_clipped+i)->contour[0].num_vertices; j++)
            {
                robot[i]->m_subspace[j].px=(voronoi_clipped+i)->contour[0].vertex[j].x;
                robot[i]->m_subspace[j].py=(voronoi_clipped+i)->contour[0].vertex[j].y;
            }
            robot[i]->m_subspace[(voronoi_clipped+i)->contour[0].num_vertices].px=robot[i]->m_subspace[0].px;
            robot[i]->m_subspace[(voronoi_clipped+i)->contour[0].num_vertices].py=robot[i]->m_subspace[0].py;
            //---------------------------

        }
        printf("\n");

        /*free(poly_border.hole);
        free(poly_border.contour);
        free(poly_border.contour[0].vertex);

        free(voronoi_cell[i].hole);
        free(voronoi_cell[i].contour);
        free(voronoi_cell[i].contour[0].vertex);*/

        /*gpc_free_polygon(poly_border);
        gpc_free_polygon(voronoi_cell+i);
        gpc_free_polygon(voronoi_clipped+i);*/

    }

    if (reached_centroids)
    {
        double min_area=1000;
        double max_area=0;
        for (i=0; i<ROBOT_QTY; i++)
        {
            if(fabs(area[i])>max_area) max_area=fabs(area[i]);
            if(fabs(area[i])<min_area) min_area=fabs(area[i]);
        }
        printf("Area deviation=%3.2f\n", max_area-min_area);
        printf("Area deviation (percents of biggest)=%3.2f\n", (max_area-min_area)/max_area*100);
        area_divided=true;
    }

    delete poly_border;
    delete[] voronoi_cell;
    delete[] voronoi_clipped;

    printf("Finished \n");
}

bool inputAvailable()
{
  struct timeval tv;
  fd_set fds;
  tv.tv_sec = 0;
  tv.tv_usec = 0;
  FD_ZERO(&fds);
  FD_SET(STDIN_FILENO, &fds);
  select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
  return (FD_ISSET(0, &fds));
}

void SetInitPose(unsigned int ip)
{
    switch(ip)
    {

    case P_OK:
        robot[0]->SetPose(-8   ,5,0);
        if(ROBOT_QTY>1) robot[1]->SetPose(-8.1 ,3.3,0);
        if(ROBOT_QTY>2) robot[2]->SetPose(-8.25,1.1,0);
        if(ROBOT_QTY>3) robot[3]->SetPose(-8.1 ,-1.1,0);
        if(ROBOT_QTY>4) robot[4]->SetPose(-8.25,-3.3,0);
        if(ROBOT_QTY>5) robot[5]->SetPose(-8.0,-5,0);
        break;

    case OD:
        robot[0]->SetPose(-8,-6,0);
        if(ROBOT_QTY>1) robot[1]->SetPose(-5,-6,0);
        if(ROBOT_QTY>2) robot[2]->SetPose(-2,-6,0);
        if(ROBOT_QTY>3) robot[3]->SetPose(2,-6,0);
        if(ROBOT_QTY>4) robot[4]->SetPose(5,-6,0);
        if(ROBOT_QTY>5) robot[5]->SetPose(8,-6,0);
        break;

    case DK:
        robot[0]->SetPose(-8,5,0);
        if(ROBOT_QTY>1) robot[1]->SetPose(-8,1,0);
        if(ROBOT_QTY>2) robot[2]->SetPose(-8,-5,0);
        if(ROBOT_QTY>3) robot[3]->SetPose(8,5,0);
        if(ROBOT_QTY>4) robot[4]->SetPose(8,1,0);
        if(ROBOT_QTY>5) robot[5]->SetPose(8,-5,0);
        break;
    case LL:
        robot[0]->SetPose(-8,-5,0);
        if(ROBOT_QTY>1) robot[1]->SetPose(-6,-5.1,0);
        if(ROBOT_QTY>2) robot[2]->SetPose(-4,-5,0);
        if(ROBOT_QTY>3) robot[3]->SetPose(-7,-3,0);
        if(ROBOT_QTY>4) robot[4]->SetPose(-5,-3.1,0);
        if(ROBOT_QTY>5) robot[5]->SetPose(-3,-3,0);
        break;
    case MA:
        printf("\n o Position Ininialization: "); fflush(stdout);
        robot[0]->SetPose(0+MIDDLE_SHIFT_X,2.59+MIDDLE_SHIFT_Y,0);
        robot_client->Read();
        printf("-"); fflush(stdout);
        if(ROBOT_QTY>1) robot[1]->SetPose(-1.44+MIDDLE_SHIFT_X,-0.45+MIDDLE_SHIFT_Y,0);
        robot_client->Read();
        printf("-"); fflush(stdout);
        if(ROBOT_QTY>2) robot[2]->SetPose(-1.65+MIDDLE_SHIFT_X,1.32+MIDDLE_SHIFT_Y,0);
        robot_client->Read();
        printf("-"); fflush(stdout);
        if(ROBOT_QTY>3) robot[3]->SetPose(0.34+MIDDLE_SHIFT_X,-1.14+MIDDLE_SHIFT_Y,0);
        robot_client->Read();
        printf("-"); fflush(stdout);
        if(ROBOT_QTY>4) robot[4]->SetPose(1.78+MIDDLE_SHIFT_X,0.05+MIDDLE_SHIFT_Y,0);
        robot_client->Read();
        printf("-"); fflush(stdout);
        if(ROBOT_QTY>5) robot[5]->SetPose(1.64+MIDDLE_SHIFT_X,1.82+MIDDLE_SHIFT_Y,0);
        robot_client->Read();
        printf(" - Finished \n"); fflush(stdout);
        printf("Please adjust robots placement and press a key...\n"); fflush(stdout);
        while (!inputAvailable())
        {
            robot_client->Read();
        }
        getchar();
        break;

        /*case RND:
            int x[ROBOT_QTY], y[ROBOT_QTY];
            int i;
            for(i=0;i<ROBOT_QTY;i++)
            {
                x[i]=(rand()%20)/2-10;
                y[i]=(rand()%14)/2-7;
            }*/

    }
}

void WaitForAKey(void)
{
    robot_client->Read();
    printf("Press a key to continue...\n"); fflush(stdout);
    while (!inputAvailable())
    {
        robot_client->Read();
    }
    getchar();
}

void ControlLoop(unsigned int i)
{
//____________________________________________________________________
// only for cooperative search

        //Init the tactic
    if (robot[i]->GetTactic()==NONE)
    {
        robot[i]->UpdateWalkSide();
        robot[i]->SelectTactic(HUNTING);
        printf("%s: start hunting.\n", robot[i]->m_name);
    }
    //Change the tactic if something detected
    if (robot[i]->GetTactic()==HUNTING)
    {

        blackboard.active[i]=TRUE;
        unsigned int l;
        double min_dist=1000, dist;
        for(l=0;l<ROBOT_QTY;l++)
        {
            if(l!=i)
            {
                dist=sqrt((robot[i]->GetX()-robot[l]->GetX())*(robot[i]->GetX()-robot[l]->GetX())+(robot[i]->GetY()-robot[l]->GetY())*(robot[i]->GetY()-robot[l]->GetY()));
                if(dist<min_dist)
                    min_dist=dist;
            }
        }
        blackboard.min_distance[i]=min_dist;

        if ((robot[i]->m_workpoint_detected>=0)&&(robot[i]->m_connected_to_workpoint<0))
        {
            if (workpoint[robot[i]->m_workpoint_detected]->IsOpen())
            {
                printf("%s: open workpoint detected.\n", robot[i]->m_name);
                robot[i]->m_wp_to_connect=robot[i]->m_workpoint_detected;
                robot[i]->SelectTactic(CONNECT_WP);
            }
        }

        if(!robot[i]->CheckFWClear())
        {
            printf("%s: robot in front of me!\n", robot[i]->m_name);
            robot[i]->SelectTactic(DECONCENTRATE);
        }

        if(robot[i]->TurnAroundStart())
        {
            robot[i]->SelectTactic(TURNAROUND);
            printf("%s: start turning around.\n", robot[i]->m_name);
        }
    }

    if (robot[i]->GetTactic()==TURNAROUND)
    {
        if(robot[i]->TurnAroundDone())
        {
            robot[i]->SelectTactic(HUNTING);
            printf("%s: turnaround done, start hunting.\n", robot[i]->m_name);
        }

        if ((robot[i]->m_workpoint_detected>=0)&&(robot[i]->m_connected_to_workpoint<0))
        {
            if (workpoint[robot[i]->m_workpoint_detected]->IsOpen())
            {
                printf("%s: open workpoint detected.\n", robot[i]->m_name);
                robot[i]->m_wp_to_connect=robot[i]->m_workpoint_detected;
                robot[i]->SelectTactic(CONNECT_WP);
                robot[i]->TurnAroundFinish();
            }
        }

    }

    if (robot[i]->GetTactic()==CONNECT_WP)
    {
        if (workpoint[robot[i]->m_workpoint_detected]->IsConnected(robot[i]->m_ID))
        {
            robot[i]->m_connected_to_workpoint=robot[i]->m_workpoint_detected;
            workpoint[robot[i]->m_connected_to_workpoint]->SetWorkpointClose(robot[i]->m_ID);
            printf("%s: connected to workpoint.\n", robot[i]->m_name);
            robot[i]->SelectTactic(STOP);
            //robot[i]->LogSuccess();
        }
        else if (!workpoint[robot[i]->m_workpoint_detected]->IsOpen())
        {
                robot[i]->m_wp_to_connect=-1;
                robot[i]->SelectTactic(HUNTING);
        }
        else
        {
            if(robot[i]->m_wp_to_connect<0)
                robot[i]->SelectTactic(HUNTING);
        }
    }

    if (robot[i]->GetTactic()==STOP)
    {
        blackboard.active[i]=FALSE;
    }

    if (robot[i]->GetTactic()==STALL_AVOID)
    {
        if (robot[i]->GetTimerInterval()>6.0)
            robot[i]->SelectTactic(robot[i]->GetPrevTactic());
    }
    //Periodically check a stall condition
    if ((robot[i]->m_stall_detected)&&(robot[i]->GetTactic()!=STALL_AVOID))
    {
        robot[i]->StartTimer();
        robot[i]->SelectTactic(STALL_AVOID);
        printf("%s: stall state detected. Avoid the stall state.\n", robot[i]->m_name);
    }
    //printf("%s: Current tactic is: %d.\n", robot[i]->m_name, robot[i]->GetTactic());

}

void ControlLoopRandomWalk(unsigned int i)
{
    if (robot[i]->GetTactic()==NONE)
    {
        robot[i]->SelectTactic(RANDOMWALK);
    }
    //Change the tactic if something detected
    if (robot[i]->GetTactic()==RANDOMWALK)
    {

        if ((robot[i]->m_workpoint_detected>=0)&&(robot[i]->m_connected_to_workpoint<0))
        {
            if (workpoint[robot[i]->m_workpoint_detected]->IsOpen())
            {
                printf("%s: open workpoint detected.\n", robot[i]->m_name);
                robot[i]->m_wp_to_connect=robot[i]->m_workpoint_detected;
                robot[i]->SelectTactic(CONNECT_WP);
            }
        }
    }

    if (robot[i]->GetTactic()==CONNECT_WP)
    {
        if (workpoint[robot[i]->m_workpoint_detected]->IsConnected(robot[i]->m_ID))
        {
            robot[i]->m_connected_to_workpoint=robot[i]->m_workpoint_detected;
            workpoint[robot[i]->m_connected_to_workpoint]->SetWorkpointClose(robot[i]->m_ID);
            printf("%s: connected to workpoint.\n", robot[i]->m_name);
            robot[i]->SelectTactic(STOP);
            //robot[i]->LogSuccess();
        }
        else
        {
            if(robot[i]->m_wp_to_connect<0)
                robot[i]->SelectTactic(RANDOMWALK);
        }
    }

    if (robot[i]->GetTactic()==STALL_AVOID)
    {
        if (robot[i]->GetTimerInterval()>6.0)
            robot[i]->SelectTactic(robot[i]->GetPrevTactic());
    }
    //Periodically check a stall condition
    if ((robot[i]->m_stall_detected)&&(robot[i]->GetTactic()!=STALL_AVOID))
    {
        robot[i]->StartTimer();
        robot[i]->SelectTactic(STALL_AVOID);
        printf("%s: stall state detected. Avoid the stall state.\n", robot[i]->m_name);
    }
    //printf("%s: Current tactic is: %d.\n", robot[i]->m_name, robot[i]->GetTactic());

}

void ControlLoopKA(unsigned int i)
{

//____________________________________________________________________
// only for TSP/KA coverage

    //Init the tactic

    int j;
    if (robot[i]->GetTactic()==NONE)
    {
        //init target points
        for (j=0; j<GRID_QTY; j++)
        {
            if((tlist[j].x==rlist[i].x)&&(tlist[j].y==rlist[i].y))
            {
                tlist[j].status=0;
            }
        }
        robot[i]->SelectTactic(KAC_ALGO);
        printf("\n%s: start KAC algo.", robot[i]->m_name);
    }
    if(robot[i]->GetTactic()==GO_TO)
    {
        if((fabs((float)rlist[i].targetX-rlist[i].x)<0.1)&&(fabs((float)rlist[i].targetY-rlist[i].y)<0.1))
        {
            free_counter++;
            rlist[i].attarget=1;
            printf("\n%s: at target point.", robot[i]->m_name);
        }

        if(free_counter==6)
        {
            for(j=0; j<ROBOT_QTY; j++)
            {
                robot[j]->SelectTactic(KAC_ALGO);
                rlist[j].free=1;
            }
        }
    }
    printf("\n%s: Current tactic is: %d", robot[i]->m_name, robot[i]->GetTactic());
}



void GridUpdate(void)
{
    unsigned int i,j;
    player_point_2d_t   point[4];
    player_color        color;

    graph2d->Clear();

    for (i=0; i<ROBOT_QTY; i++)
    {
        rlist[i].x=robot[i]->GetX();
        rlist[i].y=robot[i]->GetY();
        //printf("\n rlist.x=%f, rlist.y=%f", rlist[i].x, rlist[i].y);

        graph2d->Color(255,0,0,0);
        graph2d->DrawPolyline(robot[i]->m_subspace, robot[i]->m_subspace_order);
        graph2d->Color(0,255,0,0);
        graph2d->DrawPoints(robot[i]->m_subspace, robot[i]->m_subspace_order);
    }


    for (i=0; i<GRID_QTY; i++)
    {
        for(j=0; j<ROBOT_QTY; j++)
        {
            if((fabs((float)tlist[i].x-rlist[j].x)<COV_RADIUS)&&(fabs((float)tlist[i].y-rlist[j].y)<COV_RADIUS)&&(!tlist[i].visited))
            {
                tlist[i].visited=1;
                visited_qty++;
                if(tlist[i].status>0)
                    tlist[i].status=-1;
            }
        }
        if (!tlist[i].visited)
        {
            point[0].px=tlist[i].x-0.1;
            point[0].py=tlist[i].y-0.1;

            point[1].px=tlist[i].x+0.1;
            point[1].py=tlist[i].y-0.1;

            point[2].px=tlist[i].x+0.1;
            point[2].py=tlist[i].y+0.1;

            point[3].px=tlist[i].x-0.1;
            point[3].py=tlist[i].y+0.1;

            color.green=0;
            color.blue=0;
            color.red=127;

            graph2d->DrawPolygon(point, 4, 1, color);
        }
    }



    //printf("\nVisitedQTY=%d", visited_qty);
    if(visited_qty==tlist_counter_init)
    {
        //finish_flag=true;
    }

}

void CollisionHandler(unsigned int i)
{
    if (robot[i]->m_stallSteps>3)
    {
        robot[i]->SelectTactic(GO_BACKWARD);
        std::cout<<"stop!\n";
        robot[i]->m_stallSteps=0;
        robot[i]->m_stallCounter++;
        //finish_flag=true;
    }
}

void FinishLog(void)
{
    ftime(&tp_end);
    time(&finish_time);
    //fprintf(log_out, "----------------------\n %s \n", ctime(&finish_time));
    //fprintf(log_out, "Coverage time: %3.3f sec\n",((double)tp_end.time+(double)tp_end.millitm/1000.0)-((double)tp_start.time+(double)tp_start.millitm/1000.0));
    fprintf(log_out, "%3.3f ; ",((double)tp_end.time+(double)tp_end.millitm/1000.0)-((double)tp_start.time+(double)tp_start.millitm/1000.0));
    int i,j;
//    tlist_counter_final=0;
//    for (i=0; i<GRID_V; i++)
//    {
//        for (j=0; j<GRID_H; j++)
//        {
//            if(tlist[i*GRID_H+j].visited==0)
//                tlist_counter_final++;
//        }
//    }
//    //fprintf(log_out, "Initial target qty: %d\n", tlist_counter_init);
//    //fprintf(log_out, "Covered target qty: %d\n", tlist_counter_init-tlist_counter_final);
//    //fprintf(log_out, "Complete: %2.2f%%\n", ((float)tlist_counter_init-(float)tlist_counter_final)/(float)tlist_counter_init);
//    fprintf(log_out, "%d ; ", tlist_counter_init);
//    fprintf(log_out, "%d ; ", tlist_counter_init-tlist_counter_final);
//    fprintf(log_out, "%2.2f ; ", ((float)tlist_counter_init-(float)tlist_counter_final)/(float)tlist_counter_init);
//    printf("3");
    double travel_path=0;
    for(i=0; i<ROBOT_QTY; i++)
    {
        travel_path+=robot[i]->GetTravelPath();
    }
    //fprintf(log_out, "Total travel path: %3.3f\n", travel_path);
    //fprintf(log_out, "Avg length to target: %3.3f\n", travel_path/((float)tlist_counter_init-(float)tlist_counter_final));
    fprintf(log_out, "%3.3f \n ", travel_path);
    //fprintf(log_out, "%3.3f \n", travel_path/((float)tlist_counter_init-(float)tlist_counter_final));
}

void reset_terminal_mode()
{
    tcsetattr(0, TCSANOW, &orig_termios);
}

void set_conio_terminal_mode()
{
    struct termios new_termios;

    /* take two copies - one for now, one for later */
    tcgetattr(0, &orig_termios);
    memcpy(&new_termios, &orig_termios, sizeof(new_termios));

    /* register cleanup handler, and set the new terminal mode */
    atexit(reset_terminal_mode);
    cfmakeraw(&new_termios);
    tcsetattr(0, TCSANOW, &new_termios);
}
