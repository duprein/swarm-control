# Desc: Polygon Charger Models
# Author: Vladimir Aleksandrov
# Date: 17 Feb 2010

define charger model
(
    bitmap "bitmaps/charger.png"
    size [1.2 0.4 1]

    # determine how the model appears in various sensors

    obstacle_return 1
    laser_return 1
    ranger_return 1
    blobfinder_return 1

    gripper_return 0
    audio_return 0

    fiducial_key 0

	color "brown"

    # GUI properties
    gui_nose 0
    gui_grid 0
    gui_boundary 0

    charger_bump( pose [0 0 -0.05 90] )
)

define charger_bump fiducial
(
	range_min 0.0
   	range_max 0.06
   	range_max_id 0.06
   	fov 180
   	fiducial_key 1

	# model properties
   	size [ 0.1 0.1 0.1 ]
)
