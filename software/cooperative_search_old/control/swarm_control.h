#ifndef SWARM_CONTROL_H
#define SWARM_CONTROL_H

#include <libplayerc++/playerc++.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/timeb.h>
#include "args.h"
#include "robot_classes.h"
#include "polygon_classes.h"
#include <fstream>

#include <fuzzylite/FuzzyLite.h>

using namespace PlayerCc;

void ControlLoop(unsigned int i);
void CollisionHandler(unsigned int i);

struct positionHolder
{
    double x;
    double y;
    double yaw;
} typedef positionHolder_t;

struct swarmRobotList
{
    char                name[7];
    unsigned int        stallCounter;
    player_pose2d_t     initialPos;
    player_pose2d_t     currentPos;
    double              vel;
    double              ang;
    bool                object_detected;
    bool                charger_detected;
    unsigned int        see_object;
    unsigned int        see_charger;
    bool                charging;
    double              battery;
    uint32_t            lifeCycle;
    double              oaVel;
    double              oaAng;
    double              oaK;
} typedef swarmRobot_t;

struct  worldList
{
    uint32_t            step;
    uint32_t            epoch;
} typedef world_t;

world_t                 world;

struct objectList
{
    char                name[3];
    bool                activated;
    uint32_t            color;
    player_pose2d_t     location;
    unsigned int        activated_by;
    bool                collected;
} typedef object_t;

struct chargerList
{
    char                name[8];
    bool                occuped;
    unsigned int        occuped_by;
    player_pose2d_t     location;
    bool                charge_in_progress;
} typedef charger_t;

struct timeb tp1, tp2, tp3, tp4;


Robot       *robot[ROBOT_QTY];
Workpoint   *workpoint[WORKPOINT_QTY];
Charger     *charger[CHARGER_QTY];
Polygon     *polygon;
PlayerClient	*robot_client;
PlayerClient	*workpoints_client;
#endif // SWARM_CONTROL_H
