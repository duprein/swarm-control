#include "swarm_control.h"

int main(int argc, char *argv[])
{
    // ***********  LOCAL VARIABLES & STRUCTURES ****************
    int i;

    // ******************* INITIALIZING ********************

    robot_client = new PlayerClient("localhost", 6665);
    workpoints_client = new PlayerClient("localhost", 6664);

 for (i=0;i<WORKPOINT_QTY;i++)
    {
        printf("\n in WP init");
        workpoint[i] = new Workpoint(i, workpoints_client, robot_client);
    }

    for (i=0;i<ROBOT_QTY;i++)
    {
        robot[i] = new Robot(i, robot_client);
    }



    /*for (i=0;i<CHARGER_QTY;i++)
    {
        charger[i] = new Charger(i);
    }*/

    world.step=0;
    world.epoch=0;

    // ************************ MAIN LOOP ************************

    for (;;)
    {
        /* this blocks until new data comes; 10Hz by default */
        printf("Full Cycle Time of %d.%d: %1.3f sec\n",world.epoch,world.step,((double)tp2.time+(double)tp2.millitm/1000.0)-((double)tp1.time+(double)tp1.millitm/1000.0));
        ftime(&tp1);

        robot_client->Read();
        printf("Control Routine Time of %d.%d: %1.3f sec\n",world.epoch,world.step,((double)tp4.time+(double)tp4.millitm/1000.0)-((double)tp3.time+(double)tp3.millitm/1000.0));
        ftime(&tp3);
        for (i=0;i<ROBOT_QTY;i++)
        {
            robot[i]->StateUpdater();
            ControlLoop(i);
            robot[i]->Control();
        }

        robot[0]->PrintRanges();


        ftime(&tp4);

        workpoints_client->Read();
        for (i=0;i<WORKPOINT_QTY;i++)
        {
            workpoint[i]->StateUpdater();
        }
        world.step++;

        ftime(&tp2);
    }
}

void ControlLoop(unsigned int i)
{
    //Init the tactic
    if (robot[i]->GetTactic()==NONE)
    {
        robot[i]->SelectTactic(HUNTING);
        printf("%s: start hunting.\n", robot[i]->m_name);
    }
    //Change the tactic if something detected
    if (robot[i]->GetTactic()==HUNTING)
    {

        blackboard.active[i]=TRUE;
        unsigned int l;
        double min_dist=1000, dist;
        for(l=0;l<ROBOT_QTY;l++)
        {
            if(l!=i)
            {
                dist=sqrt((robot[i]->GetX()-robot[l]->GetX())*(robot[i]->GetX()-robot[l]->GetX())+(robot[i]->GetY()-robot[l]->GetY())*(robot[i]->GetY()-robot[l]->GetY()));
                printf("dist_%d=%2.4f\n", l, dist);
                if(dist<min_dist)
                    min_dist=dist;
            }
        }
        blackboard.min_distance[i]=min_dist;

        if ((robot[i]->m_workpoint_detected>=0)&&(robot[i]->m_connected_to_workpoint<0))
        {
            if (workpoint[robot[i]->m_workpoint_detected]->IsOpen())
            {
                printf("%s: open workpoint detected.\n", robot[i]->m_name);
                robot[i]->m_wp_to_connect=robot[i]->m_workpoint_detected;
                robot[i]->SelectTactic(CONNECT_WP);
            }
        }

        if(!robot[i]->CheckFWClear())
        {
            printf("%s: robot in front of me!\n", robot[i]->m_name);
            robot[i]->SelectTactic(DECONCENTRATE);
        }
    }

    if (robot[i]->GetTactic()==CONNECT_WP)
    {
        if (robot[i]->m_connected_to_workpoint>=0) //Serve connected state
        {
            printf("%s: connected to workpoint.\n", robot[i]->m_name);
            robot[i]->SelectTactic(STOP);
            robot[i]->LogSuccess();
        }
        else if (workpoint[robot[i]->m_workpoint_detected]->IsConnected(robot[i]->m_ID))
            {
                robot[i]->m_connected_to_workpoint=robot[i]->m_workpoint_detected;
                workpoint[robot[i]->m_connected_to_workpoint]->SetWorkpointClose(robot[i]->m_ID);
            }
        else
        {
            if(robot[i]->m_wp_to_connect<0)
                robot[i]->SelectTactic(HUNTING);
        }
    }

    if (robot[i]->GetTactic()==STOP)
    {
        blackboard.active[i]=FALSE;
    }

    if (robot[i]->GetTactic()==STALL_AVOID)
    {
        if (robot[i]->GetTimerInterval()>6.0)
            robot[i]->SelectTactic(robot[i]->GetPrevTactic());
    }
    //Periodically check a stall condition
    if ((robot[i]->m_stall_detected)&&(robot[i]->GetTactic()!=STALL_AVOID))
    {
        robot[i]->StartTimer();
        robot[i]->SelectTactic(STALL_AVOID);
        printf("%s: stall state detected. Avoid the stall state.\n", robot[i]->m_name);
    }
    printf("%s: Current tactic is: %d.\n", robot[i]->m_name, robot[i]->GetTactic());

}

void CollisionHandler(unsigned int i)
{
    if (robot[i]->m_stallSteps>3)
    {
        robot[i]->SelectTactic(GO_BACKWARD);
        std::cout<<"stop!\n";
        robot[i]->m_stallSteps=0;
        robot[i]->m_stallCounter++;

    }
}
