# Desc: Polygon Objects Models
# Author: Vladimir Aleksandrov
# Date: 17 Feb 2010

define work_point model
(
    bitmap "bitmaps/work_point.png"
    size [1.2 1.2 1]
    color_rgba [ 0.8 0.8 0.0 1.0 ]
    # determine how the model appears in various sensors

    obstacle_return 1
    laser_return 1
    ranger_return 1
    blobfinder_return 1

    gripper_return 0
    audio_return 0

    fiducial_key 0

    # GUI properties
    gui_nose 0
    gui_grid 0
    gui_boundary 0

    obj_bump( pose [0 0 -0.6 0] )
)

define obj_bump fiducial
(
	range_min 0.0
   	range_max 2
   	range_max_id 2
   	fov 360
   	fiducial_key 1

	# model properties
   	size [ 0.1 0.1 0.1 ]
)
