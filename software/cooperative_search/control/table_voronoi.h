#ifndef TABLE_VORONOI_H_INCLUDED
#define TABLE_VORONOI_H_INCLUDED

#define NDIM    2

char ch_cap ( char c );
bool ch_eqi ( char c1, char c2 );
int ch_to_digit ( char c );
int diaedg ( double x0, double y0, double x1, double y1, double x2, double y2,
  double x3, double y3 );
double *dtable_data_read ( char *input_filename, int m, int n );
void dtable_header_read ( char *input_filename, int *m, int *n );
int dtris2 ( int point_num, double point_xy[], int *tri_num,
  int tri_vert[], int tri_nabe[] );
int file_column_count ( char *input_filename );
int file_row_count ( char *input_filename );
void handle_file ( char *input_filename );
int i4_max ( int i1, int i2 );
int i4_min ( int i1, int i2 );
int i4_modp ( int i, int j );
int i4_sign ( int i );
int i4_wrap ( int ival, int ilo, int ihi );
void i4mat_transpose_print ( int m, int n, int a[], char *title );
void i4mat_transpose_print_some ( int m, int n, int a[], int ilo, int jlo,
  int ihi, int jhi, char *title );
int *i4vec_indicator ( int n );
void i4vec_print ( int n, int a[], char *title );
double *line_exp_normal_2d ( double p1[], double p2[] );
int lrline ( double xu, double yu, double xv1, double yv1, double xv2,
  double yv2, double dv );
bool perm_check ( int n, int p[] );
void perm_inv ( int n, int p[] );
double r8_epsilon ( void );
double r8_max ( double x, double y );
double r8_min ( double x, double y );
void r82vec_permute ( int n, double a[], int p[] );
int *r82vec_sort_heap_index_a ( int n, double a[] );
void r8mat_transpose_print ( int m, int n, double a[], char *title );
void r8mat_transpose_print_some ( int m, int n, double a[], int ilo, int jlo,
  int ihi, int jhi, char *title );
int s_len_trim ( char *s );
double s_to_r8 ( char *s, int *lchar, bool *error );
bool s_to_r8vec ( char *s, int n, double rvec[] );
int s_word_count ( char *s );
int swapec ( int i, int *top, int *btri, int *bedg, int point_num,
  double point_xy[], int tri_num, int tri_vert[], int tri_nabe[],
  int stack[] );
void timestamp ( void );
int tri_augment ( int v_num, int nodtri[] );
double *triangle_circumcenter_2d ( double t[] );
void vbedg ( double x, double y, int point_num, double point_xy[], int tri_num,
  int tri_vert[], int tri_nabe[], int *ltri, int *ledg, int *rtri, int *redg );
void voronoi_data ( int g_num, double g_xy[], int g_degree[], int g_start[],
  int g_face[], int *v_num, double v_xy[], int *i_num, double i_xy[] );

#endif // TABLE_VORONOI_H_INCLUDED
