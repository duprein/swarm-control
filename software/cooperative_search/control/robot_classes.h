#ifndef ROBOT_CLASSES_H
#define ROBOT_CLASSES_H

#include "swarm_control.h"
#include <stdio.h>
#include <stdlib.h>
#include <libplayerc++/playerc++.h>
#include <floatfann.h>
#include "polygon_classes.h"
#include <iostream>
#include <fstream>

#include <fuzzylite/FuzzyLite.h>
#include <limits>
#include "fuzzylite/FunctionTerm.h"

#define     LOG_FLOOD   true
#define     ROBOT_BASE  30
#define     STD_VEL		0.5
#define     STD_ANG		0.5
#define     MIN_DIST	1.2
#define     D_MAX       15
#define     COV_RADIUS  1.1

#define     GOTO_MIN_DIST   0.6

/**
Vladimir Aleksandrov
21/02/2010
*/

using namespace PlayerCc;

enum { DUMMY_OBSTACLE_AVOID     =1,
       WAIT                     =2,
       GO_FORWARD               =3,
       GO_BACKWARD              =4,
       STOP                     =5,
       FUZZY_OBSTACLE_AVOID     =6,
       STALL_AVOID              =7,
       HUNTING                  =8,
       CHECK_WP                 =9,
       CONNECT_WP               =10,
       DISCONNECT_WP            =11,
       CHECK_CHG                =12,
       CONNECT_CHG              =13,
       DISCONNECT_CHG           =14,
       NONE                     =15,
       DECONCENTRATE            =16,
       KAC_ALGO                 =17,
       GO_TO                    =18,
       TSP                      =19
     };

struct rlist
{
    float           x;
    float           y;
    unsigned char   free;
    unsigned int    attarget;
    int             targetX;
    int             targetY;
    float           targetYaw;
} typedef rlist_t;

struct stoplist
{
    unsigned char stopped;
    int caused;
} typedef stoplist_t;

stoplist_t slist[ROBOT_QTY];

rlist_t rlist[ROBOT_QTY];

struct tlist
{
    int             x;
    int             y;
    int             status;
    unsigned char   visited;
} typedef tlist_t;

tlist_t tlist[GRID_QTY];
unsigned int visited_qty;
float eff[ROBOT_QTY][GRID_QTY];

struct blackboard
{
    bool        walk_side[ROBOT_QTY];
    bool        active[ROBOT_QTY];
    double      min_distance[ROBOT_QTY];
} typedef blackboard_t;

blackboard_t    blackboard;

/**
Contains basic functions and variables that will control all robots.
*/
class Robot
{
public:


    int                 m_ID;
    char                m_name[7];
    int                 m_charger_detected;
    int                 m_workpoint_detected;
    int                 m_connected_to_workpoint;
    int                 m_wp_to_connect;

    int                 m_stallSteps;
    int                 m_stallCounter;
    int                 m_stall_detected;
    bool                m_goto_flag;
    double              m_travel_path;

    tlist_t             m_tlist[GRID_QTY];
    unsigned int        m_tqty;
    player_point_2d_t   m_subspace[10];
    unsigned int        m_subspace_order;


    struct fann     *m_ann;

    Robot(int mID,PlayerClient *client);
    ~Robot(void)
    {
        fann_destroy(m_ann);
        delete m_posProx;
        delete m_simProx;
        delete m_rangerProx;
        delete m_fidProx;
        delete m_fid_fw_Prox;
        delete m_fid_bw_Prox;
    };
    double 	        GetSpeed(void);
    double          GetRotation(void);
    double          GetX(void);
    double          GetY(void);
    void            SetLastRotation(double rot);
    void 		    ReadSensors(void);
    void		    LogPosition(FILE *fp);
    void            SetVelAng(double vel, double ang);
    void            StateUpdater(void);
    void            SelectTactic(unsigned int tactic);
    void            Control(void);
    unsigned int    GetTactic(void);
    unsigned int    GetPrevTactic(void);
    void            StartTimer(void);
    double          GetTimerInterval(void);
    void            LogSuccess(void);
    bool            CheckFWClear(void);
    bool            CheckBWClear(void);
    void            SetPose(double x, double y, double yaw);
    void            MoveTo(double x, double y, double yaw);
    void            AddTravelPath(void);
    double          GetTravelPath(void);
    void            InitPosition(void);
    void            DivideTargets(void);
    int             PipSolver(player_point_2d_t poly[10], unsigned int vertex_qty, float point_x, float point_y);

protected:
    double          m_vel; //linear velocity
    double          m_ang; //angular velocity
    double          m_prev_ang;
    double          m_maxSpeed;

    unsigned int    m_fuzzy_stall_counter;

    unsigned int    m_tactic;
    unsigned int    m_prev_tactic;

    double          m_wpc_last_ang;
    int             m_wpc_flag;
    struct timeb    time1, time2;
    unsigned int    m_cycle_counter;
    unsigned int    m_cycle_step_counter;

    unsigned int    m_decon_from;
    double          m_oldX;
    double          m_oldY;
    unsigned int    m_avoid_cycles;

    /*
    have to declare as pointers because it doesn't like constructing the objects
    without input arguements.
    */
    PlayerClient	*m_robot;
    Position2dProxy	*m_posProx;
    SimulationProxy	*m_simProx;
    FiducialProxy   *m_fidProx;
    FiducialProxy   *m_fid_fw_Prox;
    FiducialProxy   *m_fid_bw_Prox;
    SonarProxy      *m_sonProx;
    RangerProxy     *m_rangerProx;

    fl::FuzzyEngine *engine;
    fl::InputLVar   *r_a_0;
    fl::InputLVar   *r_a_1;
    fl::InputLVar   *r_a_2;
    fl::OutputLVar  *speed;
    fl::OutputLVar  *rotation;
    fl::RuleBlock   *block;

    void		    SetMotors(void);
    void            DummyObstacleAvoid(void);
    void            FuzzyObstacleAvoid(void);
    void            CooperativeSearch(void);
    void            KalyaevAreaCoverage(void);
};


/**
Constructs the Robot object. Initialises a player client and proxies for the robot
@param ID the robot's ID, must be 1 or over. This controls which index this robot operates on.
*/
Robot::Robot(int ID,PlayerClient *client)
{
    //initialises some member variables
    m_ID = ID;
    sprintf(m_name, "swarm%d", ID);
    m_vel = 0;
    m_ang = 0;
    m_stallSteps=0;
    m_stallCounter=0;
    m_stall_detected=0;
    m_connected_to_workpoint=-1;
    m_tactic=NONE;
    m_goto_flag=false;
    m_avoid_cycles=0;
    //initialise player related member variables

    printf("\n 1");

    //m_robot = new PlayerClient("localhost", 6665);
    m_robot=client;
    printf("\n 1.1");

    m_posProx = new Position2dProxy(m_robot, m_ID);
    printf("\n 1.2");
    m_simProx = new SimulationProxy(m_robot, 0);
    printf("\n 1.3");
    m_rangerProx = new RangerProxy(m_robot, m_ID);
    printf("\n 1.4");
    m_fidProx = new FiducialProxy(m_robot,m_ID);
    printf("\n 1.5");
    m_fid_fw_Prox = new FiducialProxy(m_robot,10+m_ID);
    printf("\n 1.6");
    m_fid_bw_Prox = new FiducialProxy(m_robot,20+m_ID);
    //m_sonProx = new SonarProxy(m_robot, m_ID);

    slist[m_ID].stopped=0;
    slist[m_ID].caused=-1;

    printf("\n 2");

    //m_rangerProx->Configure(-5,5,10,0,3,30,100);

    //do some initializing player stuff.
    //it's a pointer so we can't access its functions with the . operator
    m_posProx->SetMotorEnable(1);
    m_posProx->RequestGeom();
    m_robot->Read();

    printf("\n 3");
    //m_ann = fann_create_from_file("nn_ranger/trained.net");

    //FuzzyLite initializing
    engine = new fl::FuzzyEngine ( "ObstacleAvoid" );
    r_a_0 = new fl::InputLVar("Sensor0");
    r_a_0->addTerm(new fl::ShoulderTerm("NEAR", 0.0, 0.50));
    r_a_0->addTerm(new fl::TriangularTerm("MED", 0.30, 2.50));
    r_a_0->addTerm(new fl::TriangularTerm("FAR", 1.50, 5.00));
    engine->addInputLVar(r_a_0);
    r_a_1 = new fl::InputLVar("Sensor1");
    r_a_1->addTerm(new fl::ShoulderTerm("NEAR", 0.0, 0.50));
    r_a_1->addTerm(new fl::TriangularTerm("MED", 0.30, 2.50));
    r_a_1->addTerm(new fl::TriangularTerm("FAR", 1.50, 5.00));
    engine->addInputLVar(r_a_1);
    r_a_2 = new fl::InputLVar("Sensor2");
    r_a_2->addTerm(new fl::ShoulderTerm("NEAR", 0.0, 0.50));
    r_a_2->addTerm(new fl::TriangularTerm("MED", 0.30, 2.50));
    r_a_2->addTerm(new fl::TriangularTerm("FAR", 1.50, 5.00));
    engine->addInputLVar(r_a_2);

    speed = new fl::OutputLVar("Speed");
    std::vector<std::string> labels;
    labels.clear();
    labels.push_back("NL");
    labels.push_back("NS");
    labels.push_back("ZR");
    labels.push_back("PS");
    labels.push_back("PL");
    speed->createTerms(5, fl::LinguisticTerm::MF_TRIANGULAR, -0.5, 0.5, labels);
    engine->addOutputLVar(speed);

    rotation = new fl::OutputLVar("Rotation");
    labels.clear();
    labels.push_back("NL");
    labels.push_back("NS");
    labels.push_back("ZR");
    labels.push_back("PS");
    labels.push_back("PL");
    rotation->createTerms(5, fl::LinguisticTerm::MF_TRIANGULAR, -0.75, 0.75, labels);
    engine->addOutputLVar(rotation);


    block = new fl::RuleBlock();
    //block->addRule(new fl::MamdaniRule("if Sensor0 is FAR then Rotation is PS", *engine));
    block->addRule(new fl::MamdaniRule("if Sensor0 is FAR and Sensor1 is FAR and Sensor2 is FAR then Speed is PL and Rotation is ZR", *engine));
    block->addRule(new fl::MamdaniRule("if Sensor0 is MED and Sensor1 is FAR and Sensor2 is FAR then Speed is PS and Rotation is NS", *engine));
    block->addRule(new fl::MamdaniRule("if Sensor0 is NEAR and Sensor1 is FAR and Sensor2 is FAR then Speed is NS and Rotation is NS", *engine));
    block->addRule(new fl::MamdaniRule("if Sensor0 is FAR and Sensor1 is MED and Sensor2 is FAR then Speed is PS and Rotation is NS", *engine));
    block->addRule(new fl::MamdaniRule("if Sensor0 is FAR and Sensor1 is NEAR and Sensor2 is FAR then Speed is ZR and Rotation is NL", *engine));
    block->addRule(new fl::MamdaniRule("if Sensor0 is FAR and Sensor1 is FAR and Sensor2 is MED then Speed is PS and Rotation is PS", *engine));
    block->addRule(new fl::MamdaniRule("if Sensor0 is FAR and Sensor1 is FAR and Sensor2 is NEAR then Speed is ZR and Rotation is PL", *engine));

    block->addRule(new fl::MamdaniRule("if Sensor0 is FAR and Sensor1 is MED and Sensor2 is MED then Speed is PS and Rotation is ZR", *engine));
    block->addRule(new fl::MamdaniRule("if Sensor0 is MED and Sensor1 is MED and Sensor2 is MED then Speed is PS and Rotation is NL", *engine));
    block->addRule(new fl::MamdaniRule("if Sensor0 is NEAR and Sensor1 is MED and Sensor2 is MED then Speed is NS and Rotation is NS", *engine));
    block->addRule(new fl::MamdaniRule("if Sensor0 is MED and Sensor1 is FAR and Sensor2 is MED then Speed is PS and Rotation is PS", *engine));
    block->addRule(new fl::MamdaniRule("if Sensor0 is MED and Sensor1 is NEAR and Sensor2 is MED then Speed is ZR and Rotation is NL", *engine));
    block->addRule(new fl::MamdaniRule("if Sensor0 is MED and Sensor1 is MED and Sensor2 is FAR then Speed is PS and Rotation is NS", *engine));
    block->addRule(new fl::MamdaniRule("if Sensor0 is MED and Sensor1 is MED and Sensor2 is NEAR then Speed is ZR and Rotation is PL", *engine));

    block->addRule(new fl::MamdaniRule("if Sensor0 is FAR and Sensor1 is NEAR and Sensor2 is NEAR then Speed is NL and Rotation is NS", *engine));
    block->addRule(new fl::MamdaniRule("if Sensor0 is MED and Sensor1 is NEAR and Sensor2 is NEAR then Speed is NL and Rotation is NL", *engine));
    block->addRule(new fl::MamdaniRule("if Sensor0 is NEAR and Sensor1 is NEAR and Sensor2 is NEAR then Speed is NL and Rotation is NL", *engine));
    block->addRule(new fl::MamdaniRule("if Sensor0 is NEAR and Sensor1 is FAR and Sensor2 is NEAR then Speed is ZR and Rotation is PL", *engine));
    block->addRule(new fl::MamdaniRule("if Sensor0 is NEAR and Sensor1 is MED and Sensor2 is NEAR then Speed is ZR and Rotation is PS", *engine));
    block->addRule(new fl::MamdaniRule("if Sensor0 is NEAR and Sensor1 is NEAR and Sensor2 is FAR then Speed is ZR and Rotation is NL", *engine));
    block->addRule(new fl::MamdaniRule("if Sensor0 is NEAR and Sensor1 is NEAR and Sensor2 is MED then Speed is ZR and Rotation is NS", *engine));

    block->addRule(new fl::MamdaniRule("if Sensor0 is MED and Sensor1 is NEAR and Sensor2 is FAR then Speed is PS and Rotation is NL", *engine));
    block->addRule(new fl::MamdaniRule("if Sensor0 is MED and Sensor1 is FAR and Sensor2 is NEAR then Speed is PS and Rotation is PL", *engine));

    engine->addRuleBlock(block);

    if (LOG_FLOOD)
        printf("%s: initialized.\n", m_name);
    return;
}

void Robot::InitPosition(void)
{
    double x, y, yaw;

    m_simProx->GetPose2d(m_name, x, y, yaw);
    m_posProx->SetOdometry(x, y, yaw);

    m_oldX=x;
    m_oldY=y;
    m_travel_path=0;

    printf("Robot %s have a position: x = %f, y = %f, yaw = %f", m_name, m_posProx->GetXPos(), m_posProx->GetYPos(), yaw);
}

bool Robot::CheckFWClear(void)
{
    if (m_fid_fw_Prox->GetCount())
        return FALSE;
    return TRUE;
}

void Robot::AddTravelPath(void)
{
    double curX,curY;
    curX=GetX();
    curY=GetY();
    m_travel_path+=sqrt(pow((curX-m_oldX),2)+pow((curY-m_oldY),2));
    m_oldX=curX;
    m_oldY=curY;
}
double Robot::GetTravelPath(void)
{
    return m_travel_path;
}

bool Robot::CheckBWClear(void)
{
    if (m_fid_bw_Prox->GetCount())
        return FALSE;
    return TRUE;
}

unsigned int Robot::GetTactic(void)
{
    return m_tactic;
}

double Robot::GetX(void)
{
    return m_posProx->GetXPos();
}

double Robot::GetY(void)
{
    return m_posProx->GetYPos();
}

void Robot::SetPose(double x, double y, double yaw)
{
    m_simProx->SetPose2d(m_name, x, y, yaw);
}

void Robot::LogSuccess(void)
{
    /*ofstream logFile("log.txt", ios::app);
    if (!logFile)
    {
        cout<< "File not open!!!"<< endl;
        return;
    }
    logFile<<m_ID<<": "<<m_posProx->GetDataTime()<<endl;
    logFile.close();*/
    FILE * fo;
    fo = fopen("log.txt","a");
    fprintf( fo, "%s: %3.3f\n", m_name, m_posProx->GetDataTime());
    fclose(fo);
}

unsigned int Robot::GetPrevTactic(void)
{
    return m_prev_tactic;
}

void Robot::StartTimer(void)
{
    ftime(&time1);
}

double Robot::GetRotation(void)
{
    return m_ang;
}

void Robot::SetLastRotation(double rot)
{
    m_wpc_last_ang=rot;
    m_wpc_flag=0;
}

double Robot::GetTimerInterval(void)
{
    ftime(&time2);
    return (((double)time2.time+(double)time2.millitm/1000.0)-((double)time1.time+(double)time1.millitm/1000.0));
}

void Robot::FuzzyObstacleAvoid(void)
{
    float range_scan[2];
    r_a_0->setInput(m_sonProx->GetScan(0));

    if (m_sonProx->GetScan(1)<m_sonProx->GetScan(2))
    {
        range_scan[0]=m_sonProx->GetScan(1);
    }
    else
    {
        range_scan[0]=m_sonProx->GetScan(2);
        printf("%s: DEBUG: range_scan[0]=%2.2f\n", m_name, range_scan[0]);
    }

    r_a_1->setInput(range_scan[0]);

    if (m_sonProx->GetScan(7)<m_sonProx->GetScan(6))
    {
        range_scan[1]=m_sonProx->GetScan(7);
    }
    else
    {
        range_scan[1]=m_sonProx->GetScan(6);
        printf("%s: DEBUG: range_scan[1]=%2.2f\n", m_name, range_scan[1]);
    }

    r_a_2->setInput(range_scan[1]);

    engine->process();
    fl::flScalar out_vel = speed->output().defuzzify();
    fl::flScalar out_ang = rotation->output().defuzzify();

    if ((fabs(out_vel)<0.01)&&(fabs(out_ang)<0.01))
    {
        m_fuzzy_stall_counter++;
        if (m_fuzzy_stall_counter>40)
        {
            m_ang=0.5;
            m_vel=0;
        }
    }
    else
    {
        m_fuzzy_stall_counter=0;
        if ((out_vel<0.5)&&(out_vel>-0.5))
            m_vel=out_vel;
        else
        {
            if (out_vel>0)
                m_vel=0.5;
            else
                m_vel=0.5;
        }
        if ((out_ang<0.5)&&(out_ang>-0.5))
            m_ang=out_ang;
        else
        {
            if (out_ang>0)
                m_ang=0.5;
            else
                m_ang=-0.5;
        }
    }

    //printf("swarm%d: FUZZY vel=%f, ang=%f\n", m_ID, out_vel, out_ang);
}

//Obstacle avoidance routine
void Robot::DummyObstacleAvoid(void)
{
    printf("\n%s: not so dummy=) obstacle avoidance routine started",m_name);
    if (    (m_rangerProx->GetRange(6)>0.4) &&
            (m_rangerProx->GetRange(7)>1.5) &&
            (m_rangerProx->GetRange(0)>2.0) &&
            (m_rangerProx->GetRange(1)>1.5) &&
            (m_rangerProx->GetRange(2)>0.4))
    {
        m_vel=0.2;
        m_ang=0;
    }
    else
    {
        if (fabs((m_rangerProx->GetRange(1)+m_rangerProx->GetRange(2))-(m_rangerProx->GetRange(6)+m_rangerProx->GetRange(7)))<0.1)
        {
            m_vel=-0.3;
            m_ang=0.0;
        }
        else if ((m_rangerProx->GetRange(1)>m_rangerProx->GetRange(7))||
                 (m_rangerProx->GetRange(2)>m_rangerProx->GetRange(6)))
        {
            m_vel=0.05;
            m_ang=0.4;
        }
        else
        {
            m_vel=0.05;
            m_ang=-0.4;
        }
    }
    printf("\n%s m_vel=%2.1f", m_name, m_vel);
    printf("\n%s m_ang=%2.1f", m_name, m_ang);
}

void Robot::SelectTactic(unsigned int tactic)
{
    m_prev_tactic=m_tactic;
    m_tactic=tactic;
}

void Robot::DivideTargets(void)
{
    m_tqty=0;
    for(unsigned int j=0; j<GRID_QTY; j++)
    {
        if(!tlist[j].visited)
        {
            if(PipSolver(m_subspace,m_subspace_order,tlist[j].x,tlist[j].y))
            {
                m_tlist[m_tqty].x=tlist[j].x;
                m_tlist[m_tqty].y=tlist[j].y;
                m_tlist[m_tqty].status=tlist[j].status;
                m_tlist[m_tqty].visited=tlist[j].visited;
                m_tqty++;
            }
        }
    }
    printf("\n swarm %i: %d point in subspace", m_ID, m_tqty);
}

int Robot::PipSolver(player_point_2d_t poly[10], unsigned int vertex_qty, float point_x, float point_y)
{
    float x1,x2;

    /* How many times the ray crosses a line-segment */
    int crossings = 0;

    /* Iterate through each line */
    for (unsigned int i = 0; i < vertex_qty; i++ )
    {

        /* This is done to ensure that we get the same result when
           the line goes from left to right and right to left */
        if ( poly[i].px < poly[(i+1)%vertex_qty].px )
        {
            x1 = poly[i].px;
            x2 = poly[(i+1)%vertex_qty].px;
        }
        else
        {
            x1 = poly[(i+1)%vertex_qty].px;
            x2 = poly[i].px;
        }

        /* First check if the ray is possible to cross the line */
        if ( point_x > x1 && point_x <= x2 && ( point_y < poly[i].py || point_y <= poly[(i+1)%vertex_qty].py ) )
        {
            static const float eps = 0.000001;

            /* Calculate the equation of the line */
            float dx = poly[(i+1)%vertex_qty].px - poly[i].px;
            float dy = poly[(i+1)%vertex_qty].py - poly[i].py;
            float k;

            if ( fabs(dx) < eps )
            {
                k = INFINITY;   // math.h
            }
            else
            {
                k = dy/dx;
            }

            float m = poly[i].py - k * poly[i].px;

            /* Find if the ray crosses the line */
            float y2 = k * point_x + m;
            if ( point_y <= y2 )
            {
                crossings++;
            }
        }
    }

    if ( crossings % 2 == 1 )
    {
        return 1;
    }
    else return 0;
}


void Robot::CooperativeSearch(void)
{
    //Correct Side of Wall Walking Algo
    int k;
    int sum=0;
    printf("DEBUG: bb->");
    for (k=0; k<ROBOT_QTY; k++)
    {
        if (blackboard.active[k])
        {
            if (blackboard.walk_side[k])
            {
                sum++;
                printf("1 ");
            }
            else
            {
                sum--;
                printf("0 ");
            }

        }
    }
    printf("\n");
    if (sum<-1)
        blackboard.walk_side[m_ID]=TRUE;
    if (sum>1)
        blackboard.walk_side[m_ID]=FALSE;

    //Implementation of Wall Walking Algo
    printf("%s: hunting cycle is %d\n", m_name, m_cycle_counter);
    if ((m_fid_fw_Prox->GetCount())||(m_fid_bw_Prox->GetCount())||(m_cycle_counter<100))
    {
        if ((m_sonProx->GetScan(6)<0.30) ||
                (m_sonProx->GetScan(7)<1.3) ||
                (m_sonProx->GetScan(0)<1.5) ||
                (m_sonProx->GetScan(1)<1.3) ||
                (m_sonProx->GetScan(2)<0.30))
        {
            if (blackboard.walk_side[m_ID])
            {
                m_vel=0;
                m_ang=0.5;
            }
            else
            {
                m_vel=0;
                m_ang=-0.5;
            }
        }
        else
        {
            m_vel=0.5;
            m_ang=0;
        }
        if (m_cycle_counter>102)
            m_cycle_counter=0;

        m_cycle_step_counter++;

        if (m_cycle_step_counter>250)
        {
            blackboard.walk_side[m_ID]=!blackboard.walk_side[m_ID];
            m_cycle_step_counter=0;
        }
    }
    else
    {
        m_vel=0;
        m_ang=0.5;
        if (m_cycle_counter>140)
            m_cycle_counter=0;

        m_cycle_step_counter=0;
    }
    m_cycle_counter++;
}

void Robot::KalyaevAreaCoverage(void)
{
    if (rlist[m_ID].free)
    {
        float dc, Di, Di_min;
        int i,j;

        //efficiency calc
        for (i=0; i<GRID_QTY; i++)
        {
            //d(j,i)(1) - calc
            dc=sqrt(pow(((float)tlist[i].x-(float)rlist[m_ID].x), 2)+pow(((float)tlist[i].y-(float)rlist[m_ID].y),2));
            if (dc>D_MAX)
                dc=0;
            else
                dc=1-dc/D_MAX;

            //D(i)(2) - calc
            Di_min=10000;
            for (j=0; j<GRID_QTY; j++)
            {
                if (tlist[j].status<1)
                {
                    Di=sqrt(pow(((float)tlist[i].x-(float)tlist[j].x),2)+pow(((float)tlist[i].y-(float)tlist[j].y),2));
                    if (Di<Di_min)
                        Di_min=Di;
                }
            }
            if (Di_min>D_MAX)
                Di=0;
            else
                Di=1-Di_min/D_MAX;

            eff[m_ID][i]=(float)tlist[i].status*(float)rlist[m_ID].free*dc*Di;
        }
        //movement
        float max1=0;
        int num1=0;
        float max2=0;
        int num2=0;
        //find max eff for m_ID
        for (i=0; i<GRID_QTY; i++)
        {
            if (max1<eff[m_ID][i])
            {
                max1=eff[m_ID][i];
                num1=i;
            }
        }
        //check that max eff is max only for m_ID
        for (i=0; i<ROBOT_QTY; i++)
        {
            if (max2<eff[i][num1])
            {
                max2=eff[i][num1];
                num2=i;
            }
        }
        if (num2==m_ID)
        {
            tlist[num1].status=0;
            rlist[m_ID].free=0;
            rlist[m_ID].targetX=tlist[num1].x;
            rlist[m_ID].targetY=tlist[num1].y;
            rlist[m_ID].targetYaw=atan(((double)tlist[num1].y-(double)rlist[m_ID].y)/((double)tlist[num1].x-(double)rlist[m_ID].x));
            for (i=0; i<GRID_QTY; i++)
            {
                if((fabs((float)tlist[i].x-rlist[m_ID].targetX)<COV_RADIUS)&&(fabs((float)tlist[i].y-rlist[m_ID].targetY)<COV_RADIUS)&&(!tlist[i].visited))
                {
                    tlist[i].status=0;
                }
            }
            for (i=0; i<GRID_QTY; i++)
                eff[m_ID][i]=0;

            printf("\nRobot %s decide to move to: %d, %d, %f\n", m_name, tlist[num1].x, tlist[num1].y, atan(((double)tlist[num1].y-(double)rlist[m_ID].y)/((double)tlist[num1].x-(double)rlist[m_ID].x)));
            //sleep(1);
        }
    }
    else
    {
        //m_posProx->GoTo(rlist[m_ID].targetX, rlist[m_ID].targetY, rlist[m_ID].targetYaw);
        //m_simProx->SetPose2d(m_name, rlist[m_ID].targetX, rlist[m_ID].targetY, rlist[m_ID].targetYaw);
        //rlist[m_ID].free=1;
        //msleep(100);
        m_tactic=GO_TO;
        rlist[m_ID].attarget=0;
    }
}


void Robot::Control(void)
{
    switch (m_tactic)
    {
    case DUMMY_OBSTACLE_AVOID:
        DummyObstacleAvoid();
        break;
    case WAIT:
        break;
    case GO_FORWARD:
        SetVelAng(STD_VEL,0);
        break;
    case GO_BACKWARD:
        SetVelAng(-STD_VEL,0);
        break;
    case STOP:
        SetVelAng(0,0);
        break;
    case FUZZY_OBSTACLE_AVOID:
        FuzzyObstacleAvoid();
        break;
    case STALL_AVOID:
        if ((m_sonProx->GetScan(0)+m_sonProx->GetScan(2)+m_sonProx->GetScan(6))>(m_sonProx->GetScan(3)+m_sonProx->GetScan(4)+m_sonProx->GetScan(5)))
            SetVelAng(1*STD_VEL,-STD_ANG/3);
        else
            SetVelAng(-1*STD_VEL,STD_ANG/3);
        break;

    case HUNTING:
        CooperativeSearch();
        //FuzzyObstacleAvoid();
        //DummyObstacleAvoid();
        break;
    case CONNECT_WP:
        if (m_workpoint_detected==m_wp_to_connect)
        {
            SetLastRotation(m_prev_ang);
            SetVelAng(0.2,0);
            m_wpc_flag=0;
        }
        else
        {
            if (m_wpc_flag<10)
            {
                if (m_wpc_last_ang!=0)
                    SetVelAng(0.0,0.3*m_wpc_last_ang/fabs(m_wpc_last_ang));
                m_wpc_flag++;
            }
            else
            {
                if (m_wpc_last_ang>0)
                    SetVelAng(0.0,-0.3);
                if (m_wpc_last_ang<0)
                    SetVelAng(0.0,0.3);
                m_wpc_flag++;
                if (m_wpc_flag>30)
                    m_wp_to_connect=-1;
            }
        }
        break;
    case DISCONNECT_WP:
        SetVelAng(-0.2,-0.5);
        break;
    case CHECK_CHG:
        break;
    case CONNECT_CHG:
        break;
    case DISCONNECT_CHG:
        break;
    case DECONCENTRATE:
        if (m_fid_fw_Prox->GetCount())
        {
            player_fiducial_item_t fwItem;
            fwItem=m_fid_fw_Prox->GetFiducialItem(0);
            if (blackboard.active[fwItem.id-ROBOT_BASE])
            {
                printf("%s: deconcentrate \n", m_name);
                if (blackboard.walk_side[m_ID])
                {
                    m_vel=0;
                    m_ang=0.5;
                }
                else
                {
                    m_vel=0;
                    m_ang=-0.5;
                }
            }
            else
                SelectTactic(HUNTING);
        }
        else
            SelectTactic(HUNTING);
        break;

    case KAC_ALGO:
        KalyaevAreaCoverage();
        break;
    case GO_TO:

        /*
                printf("\nMoveTo intro");
                if(m_avoid_cycles==0)
                    MoveTo(rlist[m_ID].targetX, rlist[m_ID].targetY, rlist[m_ID].targetYaw);
                else
                {
                    DummyObstacleAvoid();
                    SetMotors();
                    m_avoid_cycles--;
                    printf("\ndummy cycles =%d", m_avoid_cycles);
                }
        */
        m_posProx->GoTo(rlist[m_ID].targetX, rlist[m_ID].targetY, rlist[m_ID].targetYaw);

        break;

    case TSP:
    {
        unsigned int min_t_id=0;
        if(m_tqty>0)
        {


        for(unsigned int i=0; i<m_tqty; i++)
        {
            if(((m_tlist[i].x-GetX())*(m_tlist[i].x-GetX())+(m_tlist[i].y-GetY())*(m_tlist[i].y-GetY()))
                    <
                    ((m_tlist[min_t_id].x-GetX())*(m_tlist[min_t_id].x-GetX())+(m_tlist[min_t_id].y-GetY())*(m_tlist[min_t_id].y-GetY())))
            {
                min_t_id=i;
            }
        }
        m_posProx->GoTo(m_tlist[min_t_id].x, m_tlist[min_t_id].y, atan(((double)m_tlist[min_t_id].y-(double)GetY())/((double)tlist[min_t_id].x-(double)GetX())));
        }
        else
        {
            SetVelAng(0,0);
        }
        break;
    }
    default:
        break;
    }
    AddTravelPath();
}

void Robot::MoveTo(double x, double y, double yaw)
{
    printf("\nin MoveTo");
    printf("\nragerProx ok");
    float ranges[8];
    unsigned int i;
    for(i=0; i<8; i++)
    {
        ranges[i]=m_rangerProx->GetRange(i);
        printf("\nrange[%d]=%2.1f", i, ranges[i]);
    }

    printf("\nranges readed ok");

    if ((ranges[6]<GOTO_MIN_DIST)||(ranges[7]<GOTO_MIN_DIST)||(ranges[0]<GOTO_MIN_DIST)||(ranges[1]<GOTO_MIN_DIST)||(ranges[2]<GOTO_MIN_DIST))
    {
        printf("\nSTOP!");
        printf("%d", m_ID);
        slist[m_ID].stopped=1;
        printf("1");
        if (m_fid_fw_Prox->GetCount())
        {
            player_fiducial_item_t fItem;
            unsigned int cause_num;

            fItem=m_fid_fw_Prox->GetFiducialItem(0);

            printf("2");
            cause_num=fItem.id-30;//Robot fiducial offset correction

            printf("\n%d!=%d", slist[cause_num].caused, m_ID);
            if(slist[cause_num].caused!=m_ID)
            {
                printf("\nis valid");
                slist[m_ID].caused=cause_num;

            }
            else
            {
                slist[m_ID].stopped=0;
                slist[m_ID].caused=-1;
            }

        }
        else
        {
            slist[m_ID].caused=-1;
        }
        printf("3");
        if(slist[m_ID].stopped==1)
        {
            if(slist[m_ID].caused==-1)
            {
                printf("\nDummy routine intro");
                m_avoid_cycles=20;
            }

            else
            {
                printf("4");
                printf("\nCaused by swarm%d", slist[m_ID].caused);
                printf("\nRobot swarm%d attarget flag: %d",slist[m_ID].caused, rlist[slist[m_ID].caused].attarget);
                if(rlist[slist[m_ID].caused].attarget==1)
                {
                    printf("\nFREEEEEE");
                    //here you must start obstacle avoidance routine
                    m_avoid_cycles=20;
                }
                else
                    SetVelAng(0, 0);
            }
        }
    }
    else
    {
        printf("\nMOVE!");
        m_posProx->GoTo(x, y, yaw);
    }

}

void Robot::SetVelAng(double vel, double ang)
{
    if (m_ang!=0)
        m_prev_ang=m_ang;
    m_vel = vel;
    m_ang = ang;

    SetMotors();
    return;
}

/**
Gets the robot's forward speed.
@return returns the robot's current forward speed.
*/
double Robot::GetSpeed(void)
{
    return m_vel;
}

/**
Reads in the robot sensor data from the proxies. Data is accessible within the Robot object.
*/
void Robot::ReadSensors(void)
{
    if (LOG_FLOOD)
        printf("%s:\treading sensors\n", m_name);
    m_robot->Read();
    return;
}


/**
Writes the robot's current position in the world to the given file. Coordinates and yaws are
in reference to the simulation origin. Each line is comma delimited and takes the form :<br>
'Robot::m_name', xposition, yposition, yaw\n <br>
Robot::m_name is the name of the robot in the form "robotID" where ID is the number you gave this robot on construction.
*/
void Robot::LogPosition(FILE *fp)
{
    double x, y, yaw;

    try
    {
        if (LOG_FLOOD)
            printf("%s:\tgetting position LOG\n", m_name);
        m_simProx->GetPose2d(m_name, x, y, yaw);
        fprintf(fp, "'%s', %f, %f, %f\n", m_name, x, y, yaw);
        fflush(fp);
    }
    catch (PlayerError e)
    {
        fprintf(stderr, "Logging error encountered\n");
    }
}


/**
Sets the robot's motors to the current forward speed. This WILL NOT make the robot turn.
*/
void Robot::SetMotors(void)
{
    //if (LOG_FLOOD)
    //    printf("%s:\tsetting motors\n", m_name);
    m_posProx->SetSpeed(m_vel, m_ang);
    return;
}


void Robot::StateUpdater(void)
{
    //ReadSensors();
    if(!m_goto_flag)
    {
        if (m_connected_to_workpoint<0)
        {
            if (m_fidProx->GetCount()>0)
            {
                printf("%s: see something.\n", m_name);
                player_fiducial_item_t fItem;
                fItem=m_fidProx->GetFiducialItem(0);
                if (fItem.id<10)
                {
                    m_charger_detected=-1;
                    m_workpoint_detected=fItem.id-1;
                    printf("%s: workopoint detected.\n", m_name);
                }
                else if (fItem.id>9)
                {
                    m_charger_detected=fItem.id-10;
                    m_workpoint_detected=-1;
                    printf("%s: charger detected.\n", m_name);
                }
                else
                {
                    m_charger_detected=-1;
                    m_workpoint_detected=-1;
                }
            }
            else
            {
                m_charger_detected=-1;
                m_workpoint_detected=-1;
            }
        }



        if (m_posProx->GetStall())
        {
            m_stallSteps++;
            if (m_stallSteps>5)
                m_stall_detected=1;
        }
        else
        {
            m_stallSteps=0;
            m_stall_detected=0;
        }
    }
}

//*****************************************************************
//
//                  Workpoint
//
//*****************************************************************

class Workpoint
{
public:


    int                 m_ID;
    char                m_name[3];
    int                 m_difficulty;
    int                 m_connected_qty;
    int                 m_connectors[ROBOT_QTY];
    int                 m_executors_qty;
    int                 m_open_to_connect;
    int                 m_executed_by[ROBOT_QTY];
    float               m_ready_percent;
    int                 m_ready;
    player_pose2d_t     m_location;

    Workpoint(int mID, PlayerClient	*workpoints_client, PlayerClient *sim_client);
    ~Workpoint(void) {};	//declared as empty function.
    void                StateUpdater(void);
    void                DrawState(void);
    void                Execute(int robot_ID);
    void                SetWorkpointOpen(int robot_ID);
    void                SetWorkpointClose(int robot_ID);
    int                 GetReadyState(void)
    {
        return m_ready;
    };
    float               GetReadyPercent(void)
    {
        return m_ready_percent;
    };
    bool                IsConnected(int robot_ID);
    bool                IsOpen(void)
    {
        if (m_open_to_connect==1)
            return true;
        else
            return false;
    };

protected:

    PlayerClient	*m_workpoints;
    PlayerClient	*m_sim;
    FiducialProxy	*m_fidProx;
    SimulationProxy *m_simProx;
    Graphics3dProxy *m_g3dProx;
};

Workpoint::Workpoint(int mID, PlayerClient	*workpoints_client, PlayerClient *sim_client)
{
    //initialises some member variables
    m_ID = mID;
    m_difficulty=3;
    m_executors_qty=0;
    m_open_to_connect=1;
    m_ready_percent=0;
    m_ready=0;
    sprintf(m_name, "wp%d", mID);

    //m_workpoints    = new PlayerClient("localhost", 6664);
    //m_sim           = new PlayerClient("localhost", 6665);

    m_workpoints=workpoints_client;
    m_sim=sim_client;

    m_fidProx       = new FiducialProxy(m_workpoints, m_ID);
    m_simProx       = new SimulationProxy(m_sim,0);
    m_g3dProx       = new Graphics3dProxy(m_sim,0);

    m_simProx->GetPose2d(m_name, m_location.px, m_location.py, m_location.pa);

    m_workpoints->Read();
    if (LOG_FLOOD)
        printf("%s: initialized.\n", m_name);
    return;
}

void Workpoint::DrawState(void)
{
    if (m_difficulty==0)
    {
        double r=0.1;
        player_point_3d_t pts[4];

        pts[0].px = m_location.px+r;
        pts[0].py = m_location.py+r;
        pts[0].pz = 1.1;
        pts[1].px = m_location.px-r;
        pts[1].py = m_location.py+r;
        pts[1].pz = 1.1;
        pts[2].px = m_location.px-r;
        pts[2].py = m_location.py-r;
        pts[2].pz = 1.1;
        pts[3].px = m_location.px+r;
        pts[3].py = m_location.py-r;
        pts[3].pz = 1.1;

        m_g3dProx->Color(128,255,0,0);
        m_g3dProx->Draw( PLAYER_DRAW_POLYGON, pts, 4);
    }
    if (m_difficulty>0)
    {
        double r=0.2;
        player_point_3d_t pts[4];

        pts[0].px = m_location.px+r;
        pts[0].py = m_location.py+r;
        pts[0].pz = 1.1;
        pts[1].px = m_location.px-r;
        pts[1].py = m_location.py+r;
        pts[1].pz = 1.1;
        pts[2].px = m_location.px-r;
        pts[2].py = m_location.py-r;
        pts[2].pz = 1.1;
        pts[3].px = m_location.px+r;
        pts[3].py = m_location.py-r;
        pts[3].pz = 1.1;
        m_g3dProx->Color(128,128,255,0);
        m_g3dProx->Draw( PLAYER_DRAW_POLYGON, pts, 4);
    }
}

void Workpoint::StateUpdater(void)
{
    //m_workpoints->Read();
    std::cout<<m_name<<": state updated.\n";
    //Connect
    if ((m_fidProx->GetCount()>0)&&(m_open_to_connect))
    {
        std::cout<<"!!!!!!!!!!!!!!!!!!!!!";
        player_fiducial_item_t fItem;
        int l, quantity;
        quantity=m_fidProx->GetCount();

        m_connected_qty=quantity;
        for (l=0; l<ROBOT_QTY; l++)
        {
            if (l<quantity)
            {
                fItem=m_fidProx->GetFiducialItem(l);
                m_connectors[l]=fItem.id-ROBOT_BASE;
                if (LOG_FLOOD)
                    std::cout<<m_name<<": connected with "<<m_connectors[l]<<" robot.\n";
            }
            else
                m_connectors[l]=-1;
        }
    }
    else
    {
        m_connected_qty=0;
    }

    //Execute
    if ((m_executors_qty>0)&&(m_difficulty!=0)&&(!m_open_to_connect)&&(!m_ready))
    {
        switch (m_executors_qty)
        {
        case 1:
            m_ready_percent+=0.1/m_difficulty;
            break;
        case 2:
            m_ready_percent+=0.2/m_difficulty;
            break;
        case 3:
            m_ready_percent+=0.5/m_difficulty;
            break;
        case 4:
            m_ready_percent+=1/m_difficulty;
            break;
        default:
            break;
        }
        if (m_ready_percent>=100)
            m_ready=1;
        if (LOG_FLOOD)
            std::cout<<m_name<<": ready on "<<m_ready_percent;
    }

    //Ready
    if (m_ready)
    {
        if (m_connected_qty==0)
        {
            m_difficulty=3;
            m_executors_qty=0;
            m_open_to_connect=1;
            m_ready_percent=0;
            m_ready=0;
        }
    }
}

void Workpoint::Execute(int robot_ID)
{
    if (IsConnected(robot_ID))
    {
        m_executed_by[m_executors_qty]=robot_ID;
        m_executors_qty++;
    }
}

void Workpoint::SetWorkpointOpen(int robot_ID)
{
    if (IsConnected(robot_ID))
    {
        m_open_to_connect=1;
    }
}

void Workpoint::SetWorkpointClose(int robot_ID)
{
    if (IsConnected(robot_ID))
    {
        m_open_to_connect=0;
    }
}

bool Workpoint::IsConnected(int robot_ID)
{
    int k;
    if (m_connected_qty>0)
    {
        for (k=0; k<m_connected_qty; k++)
        {
            if (m_connectors[k]==robot_ID)
                return true;
        }
    }
    return false;
}

//*****************************************************************
//
//                  CHARGER
//
//*****************************************************************

class Charger
{
public:


    int         m_ID;
    char        m_name[8];
    int         m_occuped_by;

    Charger(int mID);
    ~Charger(void) {};	//declared as empty function.
    void        StateUpdater(void);

protected:

    PlayerClient	*m_chargers;
    FiducialProxy	*m_fidProx;
    PlayerClient    *m_sim;
    SimulationProxy *m_simProx;
};

Charger::Charger(int mID)
{
    //initialises some member variables
    m_ID = mID;
    m_occuped_by=-1; //FREE!=)
    sprintf(m_name, "charger%d", m_ID);

    m_chargers = new PlayerClient("localhost", 6663);
    m_sim      = new PlayerClient("localhost", 6665);
    m_fidProx  = new FiducialProxy(m_chargers, m_ID);
    m_simProx  = new SimulationProxy(m_sim,0);
    m_chargers->Read();
    if (LOG_FLOOD)
        printf("%s: initialized.\n", m_name);
    return;
}

void Charger::StateUpdater(void)
{
    m_chargers->Read();

    if (m_fidProx->GetCount()>0)
    {
        player_fiducial_item_t fItem;
        fItem=m_fidProx->GetFiducialItem(0);
        m_occuped_by=fItem.id-ROBOT_BASE;
        if (LOG_FLOOD)
            std::cout<<m_name<<": is occuped by "<<m_occuped_by<<" robot.\n";
    }
    else
    {
        m_occuped_by=-1;
    }
}

#endif // ROBOT_CLASSES_H
