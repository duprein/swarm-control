#include "swarm_control.h"

#define NDIM    2

#define POLY_L  10
#define POLY_W  7.5

player_point_2d_t   pnt[GRID_QTY];
player_color        col;
unsigned int free_counter;
player_point_2d_t   poly[10];

int main(int argc, char *argv[])
{

    // ***********  LOCAL VARIABLES & STRUCTURES ****************
    unsigned int i;

    // ******************* INITIALIZING ********************

    finish_flag=false;
    robot_client = new PlayerClient("localhost", 6665);

    for (i=0; i<ROBOT_QTY; i++)
    {
        robot[i] = new Robot(i, robot_client);
    }

    for (i=0;i<WORKPOINT_QTY;i++)
    {
        printf("\n in WP init");
        workpoint[i] = new Workpoint(i, workpoints_client, robot_client);
    }

    SetInitPose(OK);
    //SetInitPose(OD);
    //SetInitPose(DK);
    //SetInitPose(LL);

    for (i=0; i<ROBOT_QTY; i++)
    {
        robot[i]->InitPosition();
    }

    graph2d = new Graphics2dProxy(robot_client, 0);

    world.step=0;
    world.epoch=0;

    InitRlistTlist();
    area_divided=false;

    // ************************ MAIN LOOP ************************
    ftime(&tp_start);
    log_out = fopen("log2.txt","a");

    for (;;)
    {
        /* this blocks until new data comes; 10Hz by default */
        printf("\nFull Cycle Time of %d.%d: %1.3f sec",world.epoch,world.step,((double)tp2.time+(double)tp2.millitm/1000.0)-((double)tp1.time+(double)tp1.millitm/1000.0));
        ftime(&tp1);

        robot_client->Read();

        printf("\nControl Routine Time of %d.%d: %1.3f sec",world.epoch,world.step,((double)tp4.time+(double)tp4.millitm/1000.0)-((double)tp3.time+(double)tp3.millitm/1000.0));
        ftime(&tp3);


        /*printf("\n grid update starts");

        if(!area_divided)
        {
            VoronoiDivide();
        }
        else if(!finish_flag)
        {
            for(i=0; i<ROBOT_QTY; i++)
            {
                if(robot[i]->GetTactic()!=TSP)
                {
                    robot[i]->SelectTactic(STOP);
                    robot[i]->Control();
                    printf("\n swarm %i: Subspace Order = %i", i, robot[i]->m_subspace_order);
                    robot[i]->DivideTargets();
                }
                robot[i]->DivideTargets();
                robot[i]->SelectTactic(TSP);
                robot[i]->Control();
            }

        }
        else
        {
            for(i=0; i<ROBOT_QTY; i++)
            {
                robot[i]->SelectTactic(STOP);
                robot[i]->Control();
            }
        }
        //msleep(50);

        GridUpdate();

        if(finish_flag)
        {
            FinishLog();
            return 0;
        }

        printf("\n");*/

        /*for(i=0;i<GRID_QTY;i++)
        {
            for(j=0;j<ROBOT_QTY;j++)
            {
                printf("%2.2f   ", eff[j][i]);
            }
            printf("\n");
        }*/

        printf("\nRobot Control Cycle starts");
        free_counter=0;
        for (i=0;i<ROBOT_QTY;i++)
        {
            robot[i]->StateUpdater();
            ControlLoop(i);
            robot[i]->Control();
            robot[i]->AddTravelPath();
            CollisionHandler(i);
        }
        ftime(&tp4);
        printf("\nRobot Control Cycle ends");

        workpoints_client->Read();
        for (i=0;i<WORKPOINT_QTY;i++)
        {
            workpoint[i]->StateUpdater();
        }

        world.step++;

        ftime(&tp2);
    }
}

void InitRlistTlist(void)
{
    int i,j;
    for (i=0; i<ROBOT_QTY; i++)
    {
        rlist[i].x=robot[i]->GetX();
        rlist[i].y=robot[i]->GetY();
        rlist[i].free=1;
        //printf("\n rlist.x=%f, rlist.y=%f", rlist[i].x, rlist[i].y);
    }

    for (i=0; i<GRID_V; i++)
    {
        for (j=0; j<GRID_H; j++)
        {
            if(RANDOM_TLIST==0)
            {
                tlist[i*GRID_H+j].y=-(GRID_V-1)/2+i;
                tlist[i*GRID_H+j].x=-(GRID_H-1)/2+j;
                tlist[i*GRID_H+j].status=1;
                tlist[i*GRID_H+j].visited=0;
            }
            else
            {
                tlist[i*GRID_H+j].y=-(GRID_V-1)/2+i;
                tlist[i*GRID_H+j].x=-(GRID_H-1)/2+j;
                srand(time(NULL)+clock()+rand());
                //printf("random=%d \n", rand()%100);
                if((rand()%100)>50)
                {
                    tlist[i*GRID_H+j].status=1;
                    tlist[i*GRID_H+j].visited=0;
                }
                else
                {
                    tlist[i*GRID_H+j].status=-1;
                    tlist[i*GRID_H+j].visited=1;
                }
            }
            //printf("\n%d: tlist.x=%d, tlist.y=%d", i*20+j, tlist[i*20+j].x, tlist[i*20+j].y);
        }
    }
    tlist_counter_init=0;
    for (i=0; i<GRID_V; i++)
    {
        for (j=0; j<GRID_H; j++)
        {
            if(tlist[i*GRID_H+j].visited==0)
                tlist_counter_init++;
        }
    }
}

void VoronoiDivide(void)
{
    int *g_degree;
    int *g_face;
    int g_num;
    int *g_start;
    double g_xy[ROBOT_QTY*2];
    double *i_xy;
    int v_num;
    int i_num;
    double *v_xy;
    g_num=6;

    double area[ROBOT_QTY];
    player_point_2d_t centroid[ROBOT_QTY];
    //player_point_2d_t p[12];

    int i,j;

    for(i=0; i<ROBOT_QTY; i++)
    {
        g_xy[2*i]=robot[i]->GetX();
        g_xy[2*i+1]=robot[i]->GetY();
        printf("robot[%d] at [%2.1f,%2.1f]\n", i, g_xy[2*i], g_xy[2*i+1]);
    }
    /*graph2d->Clear();
    graph2d->Color(0,0,127,0);

    for (i=0; i<6; i++)
    {
        p[i].px=g_xy[2*i];
        //g_xy[2*i]=g_xy[2*i]+10;
        p[i].py=g_xy[2*i+1];
        //g_xy[2*i+1]=g_xy[2*i+1]+10;
    }
    graph2d->DrawPoints(p, 6);*/

    i_xy = new double[NDIM*g_num];
    g_degree = new int[g_num];
    g_face = new int[6*g_num];
    g_start = new int[g_num];
    v_xy = new double[NDIM*2*g_num];

    voronoi_data ( g_num, g_xy, g_degree, g_start, g_face, &v_num, v_xy,&i_num, i_xy );
    //printf("\nv_num=%d", v_num);

    player_point_2d_t vp[g_num];
    gpc_polygon *poly_border, *voronoi_cell, *voronoi_clipped;
    poly_border = new gpc_polygon;
    voronoi_cell = new gpc_polygon[ROBOT_QTY];
    voronoi_clipped = new gpc_polygon[ROBOT_QTY];

    for (i=0; i<ROBOT_QTY; i++)
    {
        //printf("Cell %d:\n", i);
        for(j=0; j<g_degree[i]; j++)
        {
            //printf("Vertex num %d type ", g_face[g_start[i]-1+j]);
            if(g_face[g_start[i]-1+j]>0)
            {
                //printf(" norm: %3.2f, %3.2f\n", v_xy[2*(g_face[g_start[i]-1+j]-1)], v_xy[2*(g_face[g_start[i]-1+j]-1)+1]);
                double x1, y1;
                x1=v_xy[2*(g_face[g_start[i]-1+j]-1)];
                y1=v_xy[2*(g_face[g_start[i]-1+j]-1)+1];

                vp[j].px=x1;
                vp[j].py=y1;
            }
            else
            {
                printf(" inf: %3.2f, %3.2f\n", i_xy[2*(-1*g_face[g_start[i]-1+j]-v_num-1)], i_xy[2*(-1*g_face[g_start[i]-1+j]-v_num-1)+1]);
                double x1, y1;
                x1=i_xy[2*(-1*g_face[g_start[i]-1+j]-v_num-1)];
                y1=i_xy[2*(-1*g_face[g_start[i]-1+j]-v_num-1)+1];
                vp[j].px=x1*100+0.1;
                vp[j].py=y1*100+0.1;
            }
        }

        poly_border->num_contours=1;
        poly_border->hole=(int *)malloc(poly_border->num_contours*sizeof(int));
        poly_border->hole[0] = FALSE;
        poly_border->contour=(gpc_vertex_list *)malloc(poly_border->num_contours * sizeof(gpc_vertex_list));
        poly_border->contour[0].num_vertices=4;
        poly_border->contour[0].vertex=(gpc_vertex *)malloc(poly_border->contour[0].num_vertices * sizeof(gpc_vertex));

        poly_border->contour[0].vertex[0].x=-10;
        poly_border->contour[0].vertex[0].y=-7.5;
        poly_border->contour[0].vertex[1].x=-10;
        poly_border->contour[0].vertex[1].y=7.5;
        poly_border->contour[0].vertex[2].x=10;
        poly_border->contour[0].vertex[2].y=7.5;
        poly_border->contour[0].vertex[3].x=10;
        poly_border->contour[0].vertex[3].y=-7.5;

        (voronoi_cell+i)->num_contours=1;
        (voronoi_cell+i)->hole=(int *)malloc((voronoi_cell+i)->num_contours * sizeof(int));
        (voronoi_cell+i)->hole[0] = FALSE;
        (voronoi_cell+i)->contour=(gpc_vertex_list *)malloc((voronoi_cell+i)->num_contours * sizeof(gpc_vertex_list));
        (voronoi_cell+i)->contour[0].num_vertices=g_degree[i];
        (voronoi_cell+i)->contour[0].vertex=(gpc_vertex *)malloc((voronoi_cell+i)->contour[0].num_vertices * sizeof(gpc_vertex));

        for(j=0; j<g_degree[i]; j++)
        {
            (voronoi_cell+i)->contour[0].vertex[j].x=vp[j].px;
            (voronoi_cell+i)->contour[0].vertex[j].y=vp[j].py;
            printf ("%d\n", j);
        }

        gpc_polygon_clip(GPC_INT, (voronoi_cell+i), poly_border, (voronoi_clipped+i));

        printf("Initial contour num: %d ; Vert num: %d\n", (voronoi_cell+i)->num_contours, (voronoi_cell+i)->contour[0].num_vertices);
        printf("Border contour num: %d ; Vert num: %d\n", poly_border->num_contours, poly_border->contour[0].num_vertices);
        printf("Clipped contour num: %d", (voronoi_clipped+i)->num_contours);

        if((voronoi_clipped+i)->num_contours>0)
        {
            printf("Vert num: %d", (voronoi_clipped+i)->contour[0].num_vertices);
            player_point_2d_t vpn[(voronoi_clipped+i)->contour[0].num_vertices+1];
            for (j=0; j<(voronoi_clipped+i)->contour[0].num_vertices; j++)
            {
                vpn[j].px=(voronoi_clipped+i)->contour[0].vertex[j].x;
                vpn[j].py=(voronoi_clipped+i)->contour[0].vertex[j].y;
            }
            vpn[(voronoi_clipped+i)->contour[0].num_vertices].px=vpn[0].px;
            vpn[(voronoi_clipped+i)->contour[0].num_vertices].py=vpn[0].py;
            //graph2d->Color(255,0,0,0);
            //graph2d->DrawPolyline(vpn, (voronoi_clipped+i)->contour[0].num_vertices+1);
            //graph2d->Color(0,255,0,0);
            //graph2d->DrawPoints(vpn, (voronoi_clipped+i)->contour[0].num_vertices+1);

            //vpn[N]=vpn[0]...... MUST BE!
            area[i]=0;
            centroid[i].px=0;
            centroid[i].py=0;
            for(j=0; j<(voronoi_clipped+i)->contour[0].num_vertices; j++)
            {
                area[i]+=(vpn[j].px*vpn[j+1].py-vpn[j+1].px*vpn[j].py);
                centroid[i].px+=(vpn[j].px*vpn[j+1].py-vpn[j+1].px*vpn[j].py)*(vpn[j].px+vpn[j+1].px);
                centroid[i].py+=(vpn[j].px*vpn[j+1].py-vpn[j+1].px*vpn[j].py)*(vpn[j].py+vpn[j+1].py);
            }
            area[i]=area[i]/2;
            centroid[i].px=centroid[i].px/(6*area[i]);
            centroid[i].py=centroid[i].py/(6*area[i]);
            printf("area[%d]=%3.2f\n", i, area[i]);
            printf("centroid[%d].px=%3.2f\n", i, centroid[i].px);
            printf("centroid[%d].py=%3.2f\n", i, centroid[i].py);
            graph2d->Color(0,255,255,0);
            graph2d->DrawPoints(centroid, ROBOT_QTY);

            robot[i]->MoveTo(centroid[i].px, centroid[i].py, 0);
            robot[i]->AddTravelPath();
            //---------------------------------
            robot[i]->m_subspace_order=(voronoi_clipped+i)->contour[0].num_vertices+1;
            for (j=0; j<(voronoi_clipped+i)->contour[0].num_vertices; j++)
            {
                robot[i]->m_subspace[j].px=(voronoi_clipped+i)->contour[0].vertex[j].x;
                robot[i]->m_subspace[j].py=(voronoi_clipped+i)->contour[0].vertex[j].y;
            }
            robot[i]->m_subspace[(voronoi_clipped+i)->contour[0].num_vertices].px=robot[i]->m_subspace[0].px;
            robot[i]->m_subspace[(voronoi_clipped+i)->contour[0].num_vertices].py=robot[i]->m_subspace[0].py;
            //---------------------------

        }
        printf("\n");


        float tolerance=3;
        if((fabs(area[0]-area[1])<tolerance)&&
                (fabs(area[0]-area[2])<tolerance)&&
                (fabs(area[0]-area[3])<tolerance)&&
                (fabs(area[0]-area[4])<tolerance)&&
                (fabs(area[0]-area[5])<tolerance))
        {
            area_divided=true;
        }

        /*free(poly_border.hole);
        free(poly_border.contour);
        free(poly_border.contour[0].vertex);

        free(voronoi_cell[i].hole);
        free(voronoi_cell[i].contour);
        free(voronoi_cell[i].contour[0].vertex);*/

        /*gpc_free_polygon(poly_border);
        gpc_free_polygon(voronoi_cell+i);
        gpc_free_polygon(voronoi_clipped+i);*/

    }
    delete poly_border;
    delete[] voronoi_cell;
    delete[] voronoi_clipped;

    printf("Finished \n");
}


void SetInitPose(unsigned int ip)
{
    switch(ip)
    {

    case OK:
        robot[0]->SetPose(-8   ,5,0);
        robot[1]->SetPose(-8.1 ,3.3,0);
        robot[2]->SetPose(-8.25,1.1,0);
        robot[3]->SetPose(-8.1 ,-1.1,0);
        robot[4]->SetPose(-8.25,-3.3,0);
        robot[5]->SetPose(-8.0,-5,0);
        break;

    case OD:
        robot[0]->SetPose(-8,-6,0);
        robot[1]->SetPose(-5,-6,0);
        robot[2]->SetPose(-2,-6,0);
        robot[3]->SetPose(2,-6,0);
        robot[4]->SetPose(5,-6,0);
        robot[5]->SetPose(8,-6,0);
        break;

    case DK:
        robot[0]->SetPose(-8,5,0);
        robot[1]->SetPose(-8,0,0);
        robot[2]->SetPose(-8,-5,0);
        robot[3]->SetPose(8,5,0);
        robot[4]->SetPose(8,0,0);
        robot[5]->SetPose(8,-5,0);
        break;
    case LL:
        robot[0]->SetPose(-8,-5,0);
        robot[1]->SetPose(-6,-5.1,0);
        robot[2]->SetPose(-4,-5,0);
        robot[3]->SetPose(-7,-3,0);
        robot[4]->SetPose(-5,-3.1,0);
        robot[5]->SetPose(-3,-3,0);
        break;

        /*case RND:
            int x[ROBOT_QTY], y[ROBOT_QTY];
            int i;
            for(i=0;i<ROBOT_QTY;i++)
            {
                x[i]=(rand()%20)/2-10;
                y[i]=(rand()%14)/2-7;
            }*/

    }
}

void ControlLoop(unsigned int i)
{
    //Init the tactic
    int j;
    if (robot[i]->GetTactic()==NONE)
    {
        //init target points
        for (j=0; j<GRID_QTY; j++)
        {
            if((tlist[j].x==rlist[i].x)&&(tlist[j].y==rlist[i].y))
            {
                tlist[j].status=0;
            }
        }
        robot[i]->SelectTactic(KAC_ALGO);
        printf("\n%s: start KAC algo.", robot[i]->m_name);
    }
    if(robot[i]->GetTactic()==GO_TO)
    {
        if((fabs((float)rlist[i].targetX-rlist[i].x)<0.1)&&(fabs((float)rlist[i].targetY-rlist[i].y)<0.1))
        {
            free_counter++;
            rlist[i].attarget=1;
            printf("\n%s: at target point.", robot[i]->m_name);
        }

        if(free_counter==6)
        {
            for(j=0; j<ROBOT_QTY; j++)
            {
                robot[j]->SelectTactic(KAC_ALGO);
                rlist[j].free=1;
            }
        }
    }
    printf("\n%s: Current tactic is: %d", robot[i]->m_name, robot[i]->GetTactic());
}

void GridUpdate(void)
{
    unsigned int i,j;
    player_point_2d_t   point[4];
    player_color        color;

    graph2d->Clear();

    for (i=0; i<ROBOT_QTY; i++)
    {
        rlist[i].x=robot[i]->GetX();
        rlist[i].y=robot[i]->GetY();
        printf("\n rlist.x=%f, rlist.y=%f", rlist[i].x, rlist[i].y);

        graph2d->Color(255,0,0,0);
        graph2d->DrawPolyline(robot[i]->m_subspace, robot[i]->m_subspace_order);
        graph2d->Color(0,255,0,0);
        graph2d->DrawPoints(robot[i]->m_subspace, robot[i]->m_subspace_order);
    }


    for (i=0; i<GRID_QTY; i++)
    {
        for(j=0; j<ROBOT_QTY; j++)
        {
            if((fabs((float)tlist[i].x-rlist[j].x)<COV_RADIUS)&&(fabs((float)tlist[i].y-rlist[j].y)<COV_RADIUS)&&(!tlist[i].visited))
            {
                tlist[i].visited=1;
                visited_qty++;
                if(tlist[i].status>0)
                    tlist[i].status=-1;
            }
        }
        if (!tlist[i].visited)
        {
            point[0].px=tlist[i].x-0.1;
            point[0].py=tlist[i].y-0.1;

            point[1].px=tlist[i].x+0.1;
            point[1].py=tlist[i].y-0.1;

            point[2].px=tlist[i].x+0.1;
            point[2].py=tlist[i].y+0.1;

            point[3].px=tlist[i].x-0.1;
            point[3].py=tlist[i].y+0.1;

            color.green=0;
            color.blue=0;
            color.red=127;

            graph2d->DrawPolygon(point, 4, 1, color);
        }
    }



    printf("\nVisitedQTY=%d", visited_qty);
    if(visited_qty==tlist_counter_init)
    {
        finish_flag=true;
    }

}

void CollisionHandler(unsigned int i)
{
    if (robot[i]->m_stallSteps>3)
    {
        /*robot[i]->SelectTactic(GO_BACKWARD);
        std::cout<<"stop!\n";
        robot[i]->m_stallSteps=0;
        robot[i]->m_stallCounter++;*/
        finish_flag=true;
    }
}

void FinishLog(void)
{
    ftime(&tp_end);
    time(&finish_time);
    //fprintf(log_out, "----------------------\n %s \n", ctime(&finish_time));
    //fprintf(log_out, "Coverage time: %3.3f sec\n",((double)tp_end.time+(double)tp_end.millitm/1000.0)-((double)tp_start.time+(double)tp_start.millitm/1000.0));
    fprintf(log_out, "%3.3f ; ",((double)tp_end.time+(double)tp_end.millitm/1000.0)-((double)tp_start.time+(double)tp_start.millitm/1000.0));
    int i,j;
    tlist_counter_final=0;
    for (i=0; i<GRID_V; i++)
    {
        for (j=0; j<GRID_H; j++)
        {
            if(tlist[i*GRID_H+j].visited==0)
                tlist_counter_final++;
        }
    }
    //fprintf(log_out, "Initial target qty: %d\n", tlist_counter_init);
    //fprintf(log_out, "Covered target qty: %d\n", tlist_counter_init-tlist_counter_final);
    //fprintf(log_out, "Complete: %2.2f%%\n", ((float)tlist_counter_init-(float)tlist_counter_final)/(float)tlist_counter_init);
    fprintf(log_out, "%d ; ", tlist_counter_init);
    fprintf(log_out, "%d ; ", tlist_counter_init-tlist_counter_final);
    fprintf(log_out, "%2.2f ; ", ((float)tlist_counter_init-(float)tlist_counter_final)/(float)tlist_counter_init);
    printf("3");
    double travel_path=0;
    for(i=0; i<ROBOT_QTY; i++)
    {
        travel_path+=robot[i]->GetTravelPath();
    }
    //fprintf(log_out, "Total travel path: %3.3f\n", travel_path);
    //fprintf(log_out, "Avg length to target: %3.3f\n", travel_path/((float)tlist_counter_init-(float)tlist_counter_final));
    fprintf(log_out, "%3.3f ; ", travel_path);
    fprintf(log_out, "%3.3f \n", travel_path/((float)tlist_counter_init-(float)tlist_counter_final));
}


