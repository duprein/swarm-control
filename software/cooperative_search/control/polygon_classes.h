#ifndef POLYGON_CLASSES_H
#define POLYGON_CLASSES_H

#include <stdio.h>
#include <stdlib.h>
#include <libplayerc++/playerc++.h>
#include "robot_classes.h"

#define ROBOT_QTY   	    6
#define WORKPOINT_QTY       4
#define CHARGER_QTY         6
#define GRID_V              13
#define GRID_H              17
#define GRID_QTY            GRID_V*GRID_H

using namespace PlayerCc;

class Polygon
{
public:

    char            m_name[7];
                    Polygon(void);
                    ~Polygon(void){};

protected:

    PlayerClient	*m_polygon;
    SimulationProxy	*m_simProx;
//  Robot           *m_robot[ROBOT_QTY];
//  Workpoint       *m_workpoint[WORKPOINT_QTY];
//  Charger         *m_charger[CHARGER_QTY];
};

Polygon::Polygon(void)
{
    return;
}

#endif // POLYGON_CLASSES_H
