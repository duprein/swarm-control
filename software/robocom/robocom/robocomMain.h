/***************************************************************
 * Name:      robocomMain.h
 * Purpose:   Defines Application Frame
 * Author:    duprein (duprein@gmail.com)
 * Created:   2011-11-16
 * Copyright: duprein ()
 * License:
 **************************************************************/

#ifndef ROBOCOMMAIN_H
#define ROBOCOMMAIN_H



#include "robocomApp.h"


#include "GUIFrame.h"

class robocomFrame: public GUIFrame
{
    public:
        robocomFrame(wxFrame *frame);
        ~robocomFrame();
    private:
        virtual void OnClose(wxCloseEvent& event);
        virtual void OnQuit(wxCommandEvent& event);
        virtual void OnAbout(wxCommandEvent& event);
};

#endif // ROBOCOMMAIN_H
