/***************************************************************
 * Name:      robocomApp.cpp
 * Purpose:   Code for Application Class
 * Author:    duprein (duprein@gmail.com)
 * Created:   2011-11-16
 * Copyright: duprein ()
 * License:
 **************************************************************/

#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif //__BORLANDC__

#include "robocomApp.h"
#include "robocomMain.h"

robocomFrame *frame;

IMPLEMENT_APP(robocomApp);

bool robocomApp::OnInit()
{
    render_loop_on = false;

    frame = new robocomFrame(0L);
    frame->Show();

    ACMinput = fopen("/dev/ttyACM0", "r");
    if (!ACMinput)
    {
        frame->statusBar->SetStatusText(_("Failed to open /dev/ttyACM0!"), 0);
        port_ok=false;
    }
    else
    {
        frame->statusBar->SetStatusText(_("/dev/ttyACM0 Opened!"), 0);
        port_ok=true;
    }
    activateRenderLoop(true);
    return true;
}

void robocomApp::activateRenderLoop(bool on)
{
    if(on && !render_loop_on)
    {
        Connect( wxID_ANY, wxEVT_IDLE, wxIdleEventHandler(robocomApp::onIdle) );
        render_loop_on = true;
    }
    else if(!on && render_loop_on)
    {
        Disconnect( wxEVT_IDLE, wxIdleEventHandler(robocomApp::onIdle) );
        render_loop_on = false;
    }
}

void robocomApp::onIdle(wxIdleEvent& evt)
{
    if(render_loop_on)
    {
        static int i=0;
        static int j;
        //frame->statusBar->SetStatusText(_("Renderer ok!"), 1);
        evt.RequestMore(); // render continuously, not only once on idle

        /***************** PORT *************/
        if(port_ok)
        {
            for (j=0; j<256; j++)
            {
                fread(&buf[j], 1, 1, ACMinput);
                if (buf[j]=='\r')
                {
                    sscanf(buf, "%i %i %i %i %i %i %i %i \r\n", &ranger[0],&ranger[1],&ranger[2],&ranger[3],&ranger[4],&ranger[5],&ranger[6],&ranger[7]);
                    frame->m_gauge1->SetValue(ranger[0]);
                    frame->m_gauge2->SetValue(ranger[1]);
                    frame->m_gauge3->SetValue(ranger[2]);
                    frame->m_gauge4->SetValue(ranger[3]);
                    frame->m_gauge5->SetValue(ranger[4]);
                    frame->m_gauge6->SetValue(ranger[5]);
                    frame->m_gauge7->SetValue(ranger[6]);
                    frame->m_gauge8->SetValue(ranger[7]);
                    frame->statusBar->SetStatusText(wxString::Format(_("Cycle: %i"), ranger[0]), 1);
                    memset(buf, 0, 256);
                }

            }




            //frame->statusBar->SetStatusText(wxString::Format(_("Cycle: %i"), i), 1);
            i++;
            //usleep(100000);
        }
        else
        {
            frame->m_gauge1->SetValue(10);
            frame->m_gauge2->SetValue(20);
            frame->m_gauge3->SetValue(30);
            frame->m_gauge4->SetValue(40);
            frame->m_gauge5->SetValue(50);
            frame->m_gauge6->SetValue(60);
            frame->m_gauge7->SetValue(70);
            frame->m_gauge8->SetValue(80);
        }

    }
}

