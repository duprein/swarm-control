/***************************************************************
 * Name:      robocomApp.h
 * Purpose:   Defines Application Class
 * Author:    duprein (duprein@gmail.com)
 * Created:   2011-11-16
 * Copyright: duprein ()
 * License:
 **************************************************************/

#ifndef ROBOCOMAPP_H
#define ROBOCOMAPP_H

#include <wx/app.h>
#include "robocomMain.h"


class robocomApp : public wxApp
{
    bool render_loop_on;
    bool port_ok;
    FILE *ACMinput;
    unsigned int ranger[8];
    char buf[256];

    public:
        virtual bool OnInit();
        void onIdle(wxIdleEvent& evt);
        void activateRenderLoop(bool on);
};

#endif // ROBOCOMAPP_H
